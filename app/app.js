// require app styles
require('./sass/main.scss');

// require vendor dependencies
let angular = require('angular');
require('angular-local-storage');
require('velocity-animate');
require('angular-cloudinary');
require('ng-file-upload');
require('moment');
require('angular-moment');

// polyfill the Promise global
require('es6-promise').polyfill();

// require vendors
require('script!../lib/angular-slider/build/angular-slider.min.js');
require('script!../lib/angular-touch/angular-touch.min.js');
require('script!../lib/medium-editor/dist/js/medium-editor.min.js');
require('script!../lib/angular-medium-editor/dist/angular-medium-editor.min.js');
require('script!../lib/angular-animate/angular-animate.min.js');
require('script!./components/angular-ui-bootstrap/ui-bootstrap-custom-tpls-1.2.5.min');

let app = angular.module('gameApp', [

	// vendor dependencies
	'vr.directives.slider',
	'angular-medium-editor',
	'LocalStorageModule',
	'ui.bootstrap',
	'ngFileUpload',
	'angular-cloudinary',
	'angularMoment',
	require('angular-ui-router'),
	require('svg-icons'),
	require('angular-formly'),
	require('angular-formly-templates-bootstrap'),
	require('angular-elastic'),
	require('angular-sanitize')

	// app components
	//'modalDialog'
]);

// app components
require('./filters')(app);
require('./services')(app);
require('./core')(app);
require('./components')(app);
require('./pages')(app);

// manually set prerendering service
window.prerenderReady = false;