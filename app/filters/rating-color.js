let _isUndefined = require('lodash/isUndefined');

module.exports = function() {
	return rating => {
		if (!_isUndefined(rating)) {
			if (rating >= 9) {
				return '#8acd80';
			} else if (rating >= 8) {
				return '#a3c579';
			} else if (rating >= 7) {
				return '#bbbd72';
			} else if (rating >= 6) {
				return '#d4b46b';
			} else if (rating >= 5) {
				return '#f8a961';
			} else if (rating >= 4) {
				return '#ff9e5f';
			} else if (rating >= 3) {
				return '#ff925f';
			} else if (rating >= 2) {
				return '#ff855f';
			} else if (rating >= 1) {
				return '#ff785f';
			} else {
				return '#ff6c5f';
			}
		} else {
			return rating;
		}
	};
};