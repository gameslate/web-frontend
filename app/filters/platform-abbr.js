module.exports = function() {
	return function(platform) {
		if (platform) {
			switch (platform) {
				case 'Super Nintendo Entertainment System':
					return 'SNES';
				case 'Nintendo Entertainment System':
					return 'NES';
				default:
					return platform;
			}
		} else {
			return platform;
		}
	}
};