module.exports = function($filter) {
	"ngInject";
	return date => {
		return $filter('date')(date, 'MMM d, yyyy');
	};
};