// scaling for cloudinary images
module.exports = function() {
	return function(image, scale) {
		if (image && image.indexOf('cloudinary') > -1) {

			// add scaling to the cloudinary url
			let word = 'upload';
			let index = (image.indexOf(word)) + word.length;
			image = image.slice(0, index) + '/c_scale,w_' + (__DEV__ ? '100' : scale) + image.slice(index);

			// if the image isn't secure, manually added the 's'
			if (image.indexOf('https') == -1) {
				word = 'http';
				index = (image.indexOf(word)) + word.length;
				image = image.slice(0, index) + 's' + image.slice(index)
			}
			return image;
		} else {
			return image;
		}
	};
};