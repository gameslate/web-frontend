let _isNumber =require('lodash/isNumber');

module.exports =  function() {
	return input => {
		if (_isNumber(input)) {
			return Math.ceil(input / 10) * 10
		} else {
			return input;
		}
	};
};