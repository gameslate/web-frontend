module.exports = function(app) {
	app.filter('roundTenth', require('./round-tenth'));
	app.filter('sassDarken', require('./sass-darken'));
	app.filter('ratingColor', require('./rating-color'));
	app.filter('defaultDate', require('./default-date'));
	app.filter('scaleImage', require('./scale-image'));
	app.filter('platformAbbr', require('./platform-abbr'));
};