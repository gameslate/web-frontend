module.exports = {
	"id": "4a772b6d-ba26-4833-b0c8-101837ed254b",
	"name": null,
	"uri": null,
	"author": {
		"id": 123,
		"name": "Tony Lefler",
		"handle": "@geoctrl",
		"profileImgUrl": "https://s.gravatar.com/avatar/248786f3c32e4a89f25b18af88e775e5?s=80&r=g"
	},
	"lead": null,
	"preview": null,
	//"coverImgUrl": 'https://i.kinja-img.com/gawker-media/image/upload/thdag0akaxtpjdgwh8lx.jpg',
	coverImgUrl: null,
	body: null,
	"tags": [
		{
			id: 1234,
			type: 'media',
			key: 'game',
			name: 'Game',
			abbr: null,
			val: 'Batman: Arkham Knight'
		},
		{
			id: 1235,
			type: 'category',
			key: 'gameplay',
			name: 'Gameplay',
			abbr: null,
			val: 'boring'
		},
		{
			id: 1236,
			type: 'category',
			key: 'graphics',
			name: 'Graphics',
			abbr: null,
			val: 'Awesome'
		},
		{
			id: 1235,
			type: 'genre',
			key: 'open-world',
			name: 'Open World',
			abbr: null,
			val: 'In-Depth'
		},
		{
			id: 1237,
			type: null,
			key: null,
			name: null,
			abbr: null,
			val: 'Crazy Cool Game'
		}
	],
	"published": null,
	"viewed": 0,
	"rating": {
		"total": 0,
		"up": 0,
		"down": 0,
		"userRating": null
	},
	"game": {
		"id": "682657fa-88e2-4f6e-893d-610299ece34e",
		"title": "Batman: Arkham Knight",
		"uri": "batman-arkham-knight"
	}
};