module.exports = {
	"id": "682657fa-88e2-4f6e-893d-610299ece34e",
	"title": "Batman: Arkham Knight",
	"uri": "batman-arkham-knight",
	"coverImgUrl": null,
	"description": "Lorem ipsum dolor sit amet, vim ad agam persius interpretaris, vis eu alienum voluptua recusabo. Quot congue qui ut, verear laoreet ceteros qui at. At his eligendi perpetua, eu vitae voluptatum eos, et illud blandit reprimique vis. Quo id summo debitis, has offendit efficiendi temporibus an, cum equidem ancillae at. Pro ea laoreet facilisi, qui illum labores convenire ad, sit ea numquam postulant. Sea te habeo euismod.Lorem ipsum dolor sit amet, vim ad agam persius interpretaris, vis eu alienum voluptua recusabo. Quot congue qui ut, verear laoreet ceteros qui at. At his eligendi perpetua, eu vitae voluptatum eos, et illud blandit reprimique vis. Quo id summo debitis, has offendit efficiendi temporibus an, cum equidem ancillae at. Pro ea laoreet facilisi, qui illum labores convenire ad, sit ea numquam postulant. Sea te habeo euismod.",
	"images": [
		{
			"url": "http://cdn3.denofgeek.us/sites/denofgeekus/files/styles/insert_main_wide_image/public/arkham_knight_1.jpg?itok=f7luAxX8",
			"title": "Batman doing his thing"
		},
		{
			"url": "http://www.push-start.co.uk/wp-content/uploads/2014/03/Batman_Arkham_Knight_screenshots011.jpg",
			"title": "Batman doing his thing"
		},
		{
			"url": "http://images.techtimes.com/data/images/full/14556/batman-arkham-knight.jpg?w=600",
			"title": "Batman doing his thing"
		}
	],
	"coOp": 0,
	"genre": [
		{
			"id": 5,
			"name": "Action"
		},
		{
			"id": 6,
			"name": "Adventure"
		}
	],
	"esrb": {
		"abbrv": "m",
		"label": "Mature",
		"rating": [
			"Blood",
			"Language",
			"Suggestive Themes",
			"Violence"
		],
		"other": "Online Interactions Not Rated by the ESRB"
	},
	"platform": [
		{
			"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
			"name": "Playstation 4",
			"abbrv": "PS4",
			"releaseDate": "2015-10-03T19:46:37.280Z",
			"developers": [
				{
					"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
					"name": "Warner Home Video ps4"
				}
			],
			"publishers": [
				{
					"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
					"name": "EA Games"
				}
			]
		},
		{
			"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
			"name": "Xbox One",
			"abbrv": "Xbox One",
			"releaseDate": "2015-10-03T19:46:37.280Z",
			"developers": [
				{
					"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
					"name": "Warner Home Video xbox"
				}
			],
			"publishers": [
				{
					"id": "9930c1fd-1ea2-4b7e-b66f-432a292173ed",
					"name": "EA Games"
				}
			]
		}

	],
	"freeToPlay": false,
	"user": {
		"rating": {
			"total": 88,
			"gameplay": 92,
			"musicSound": 76,
			"controls": 94,
			"story": 78,
			"graphics": 98,
			"replayValue": 66
		},
		"playTime": 20,
		"tags": [
			"Tony was here"
		]
	}
};