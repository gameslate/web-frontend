module.exports = [
	{
		type: 'media',
		data: [
			{
				key: 'game',
				abbr: null,
				name: 'Game'
			},
			{
				key: 'slate',
				abbr: null,
				name: 'Slate'
			},
			{
				key: 'video',
				abbr: null,
				name: 'Video'
			}
		]
	},
	{
		type: 'genre',
		data: [
			{
				key: 'open-world',
				abbr: null,
				name: 'Open World'
			},
			{
				key: 'first-person-shooter',
				abbr: 'FPS',
				name: 'First Person Shooter'
			}
		]
	}
];