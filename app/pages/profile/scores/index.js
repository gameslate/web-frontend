module.exports = function(app) {
	app.config(require('./config'));
	app.directive('profileScores', require('./scores'));
};