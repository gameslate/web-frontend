module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('profile.scores', {
				url: '/scores',
				controller: function($scope, user) {
					$scope.user = user;
				},
				template: '<profile-scores user="user"></profile-scores>'
			})
};
