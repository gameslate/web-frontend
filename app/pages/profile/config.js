module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('profile', {
				url: '/profile/:username',
				abstract: true,
				controller: function($scope, user) {
					$scope.user = user;
				},
				template: '<profile user="user"></profile>',
				resolve: {
					user: function($stateParams, userSvc) {
						return userSvc.get($stateParams.username);
					}
				}
			})
};
