module.exports = function() {

	let profileCtrl = function($rootScope, $state, authSvc, headerSvc) {
		"ngInject";

		let vm = this;
		vm.currentState = $state.current.name;
		vm.signedIn = false;

		headerSvc.set({
			title: 'Profile'
		});

		$rootScope.$on('$stateChangeSuccess', function(e, state) {
			vm.currentState = state.name;
		});

		if (authSvc.isAuthenticated()) {
			vm.signedIn = true;
		}
	};

	return {
		restrict: 'E',
		scope: {
			user: '='
		},
		template: require('./profile.html'),
		controller: profileCtrl,
		controllerAs: 'profile',
		bindToController: true
	};
};