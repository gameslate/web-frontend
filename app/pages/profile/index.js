module.exports = function(app) {
	app.config(require('./config'));
	app.directive('profile', require('./profile'));
	
	require('./recent-activity')(app);
	require('./scores')(app);
	require('./slates')(app);
	require('./wishlist')(app);
};