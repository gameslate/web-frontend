module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('profile.slates', {
				url: '/slates',
				controller: function($scope, user) {
					$scope.user = user;
				},
				template: '<profile-slates user="user"></profile-slates>'
			})
};
