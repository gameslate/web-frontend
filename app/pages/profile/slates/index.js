module.exports = function(app) {
	app.config(require('./config'));
	app.directive('profileSlates', require('./slates'));
};