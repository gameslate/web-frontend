import state from '../../../core/state';

module.exports = function() {

	let ctrl = function($scope, slateSvc, userSvc, authSvc) {
		"ngInject";
		
		let vm = this;
		vm.user = $scope.user; // profile user
		vm.state = new state($scope, vm);
		vm.state.set({
			drafts: [],
			slates: [],
			component: 'loading'
		});

		// check if logged in
		// check if if you're looking at yourself
		if (authSvc.isAuthenticated() && userSvc.isMe(vm.user._id)) {
			slateSvc.getAllDrafts().then(
					res => {
						vm.state.set({
							drafts: res,
							draftsComponent: res.docs.length?'hasData':'noData'
						});
					}
			);
		}

		userSvc.getSlates(vm.user._id).then(
				res => {
					vm.state.set({
						slates: res.docs,
						slatesComponent: res.docs.length?'hasData':'noData'
					})
				}
		);
	};

	let template = `
		<div ng-if="profileSlates.state.drafts.length || profileSlates.state.slates.length">
			<s-grid>
	      <slate-box slate="draft"
	                 ng-repeat="draft in profileSlates.state.drafts.docs"
	                 draft="true" s-grid-item>
	     </slate-box>
	     <slate-box slate="slate"
	                ng-repeat="slate in profileSlates.state.slates"
	                s-grid-item>
	     </slate-box>
	    </s-grid>
		</div>
	`;

	return {
		restrict: 'E',
		controller: ctrl,
		controllerAs: 'profileSlates',
		bindToController: true,
		template: template
	};
};