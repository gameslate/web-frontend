module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('profile.wishlist', {
				url: '/wishlist',
				controller: function($scope, user) {
					$scope.user = user;
				},
				template: '<profile-wishlist user="user"></profile-wishlist>'
			})
};
