import state from '../../../core/state';

module.exports = function() {

	let ctrl = function($scope) {
		"ngInject";
		let vm = this;
		vm.state = new state($scope, vm);
		vm.user = $scope.user;
		vm.state.set({
			wishlist: vm.user.wishlist
		});

	};

	let template = `
		<div ng-if="profileWishlist.state.wishlist">
			<s-grid col-width="100">
				<game-box data="game"
									ng-repeat="game in profileWishlist.state.wishlist"
									s-grid-item>
				</game-box>
			</s-grid>
		</div>
	`;

	return {
		restrict: 'E',
		controller: ctrl,
		controllerAs: 'profileWishlist',
		template: template
	};
};