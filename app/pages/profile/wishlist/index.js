module.exports = function(app) {
	app.config(require('./config'));
	app.directive('profileWishlist', require('./wishlist'));
};