import recentActivityMap from './recent-activity-map';

module.exports = function(app) {
	app.config(require('./config'));
	app.directive('profileRecentActivity', require('./recent-activity'));
	app.directive('recentActivityMap', recentActivityMap);
};