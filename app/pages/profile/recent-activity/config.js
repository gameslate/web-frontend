module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('profile.recentActivity', {
				url: '/recent-activity',
				controller: function($scope, user) {
					$scope.user = user;
				},
				template: '<profile-recent-activity user="user"></profile-recent-activity>'
			})
};
