import state from '../../../core/state';

module.exports = function() {

	let ctrl = function(userSvc, $scope, $state, authSvc) {
		"ngInject";
		
		let vm = this;
		vm.state = new state($scope, vm);
		vm.user = $scope.user; // from the parent controller
		vm.state.set({
			activity: []
		});

		if (authSvc.isAuthenticated()) {
			userSvc.activityFeed(vm.user._id).then(
					res => {
						vm.state.set({
							activity: res
						});
					}
			)
		} else {
			$state.go('profile.slates')
		}
	};

	let template = `
		<div class="activity-list">
			<div class="activity-list-item" ng-if="recentActivity.state.activity.docs.length === 0">
				<h4>No activity.</h4>
			</div>
			<div ng-if="recentActivity.state.activity.docs.length > 0"
					 ng-repeat="a in recentActivity.state.activity.docs"
					 recent-activity-map="a"
					 class="activity-list-item"></div>
		</div>
	`;

	return {
		restrict: 'E',
		controller: ctrl,
		controllerAs: 'recentActivity',
		bindToController: true,
		template: template
	};
};