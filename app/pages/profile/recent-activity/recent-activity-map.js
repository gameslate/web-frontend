import _get from 'lodash/get';

export default function() {

	let ctrl = function($state) {
		"ngInject";

		let vm = this;

		switch(vm.activity.action) {
			case 'addedToWishlist':
				vm.verse1 = 'Added';
				vm.verse2 = 'to your wishlist';
				vm.item = vm.activity.game.title;
				vm.link = $state.href('game', {gameUri: vm.activity.game.uri});
				break;
			case 'deletedFromWishlist':
				vm.verse1 = 'Deleted';
				vm.verse2 = 'from your wishlist';
				vm.item = vm.activity.game.title;
				vm.link = $state.href('game', {gameUri: vm.activity.game.uri});
				break;
			case 'publishedReview':
				vm.verse1 = 'Published';
				vm.item = `"${vm.activity.review.title}"`;
				vm.link = $state.href('slate', {slateUri: vm.activity.review.uri, username: vm.activity.review.author.username});
				break;
			case 'upVoted':
				vm.item = `"${vm.activity.review.title}"`;
				vm.link = $state.href('slate', {slateUri: vm.activity.review.uri, username: vm.activity.review.author.username});
				vm.by =  `@${vm.activity.review.author.username}`;
				vm.byLink = $state.href('profile.recentActivity', {username: vm.activity.review.author.username});
				break;
			case 'downVoted':
				vm.item = `"${vm.activity.review.title}"`;
				vm.link = $state.href('slate', {slateUri: vm.activity.review.uri, username: vm.activity.review.author.username});
				vm.by =  `@${vm.activity.review.author.username}`;
				vm.byLink = $state.href('profile.recentActivity', {username: vm.activity.review.author.username});
				break;
			case 'nullVoted':
				vm.verse1 = 'Removed rating from';
				vm.item = `"${vm.activity.review.title}"`;
				vm.link = $state.href('slate', {slateUri: vm.activity.review.uri, username: vm.activity.review.author.username});
				vm.by =  `@${vm.activity.review.author.username}`;
				vm.byLink = $state.href('profile.recentActivity', {username: vm.activity.review.author.username});
				break;
			case 'scoredGame':
				vm.verse1 ='Scored the game';
				vm.item = vm.activity.game.title;
				vm.link = $state.href('game', {gameUri: vm.activity.game.uri});
				break;
			case 'createDraft':
				vm.verse1 ='Created a slate draft ';
				vm.item = _get(vm.activity.review, 'title', '');
				break;
			case 'editScoredGame':
				vm.verse1 ='Edited game score';
				vm.item = _get(vm.activity.game, 'title', '');
				vm.link = $state.href('game', {gameUri: vm.activity.game.uri});
				break;
		}
	};

	let template = `
		<div class="activity-list-item__date" am-time-ago="ram.activity.date"></div>
		<div class="activity-list-item__verse ellipsis">
			<span ng-show="ram.activity.action == 'upVoted'"><icon icon="thumb-up"></icon></span>
			<span ng-show="ram.activity.action == 'downVoted'"><icon icon="thumb-down"></icon></span>
			<span>{{ram.verse1}} <a ng-href="{{ram.link}}">{{ram.item}}</a> {{ram.verse2}} <span ng-show="ram.by">by <a ng-href="{{ram.byLink}}">{{ram.by}}</a></span></span>			
		</div>
	`;

	return {
		restrict: 'A',
		scope: {
			activity: '=recentActivityMap'
		},
		controller: ctrl,
		controllerAs: 'ram',
		bindToController: true,
		template: template
	};
}