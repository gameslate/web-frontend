import * as api from '../../../core/api';

module.exports = function() {
	let ctrl = function(toasterSvc) {
		"ngInject";
		let vm = this;
		vm.state = {
			newGame: false,
			ID: '',
			coverImgUrl: '',
			Image1: '',
			Image2: '',
			Image3: ''
	};
		vm.submit = () => {
			if (vm.state.ID && vm.state.coverImgUrl) {
				api.prod.post(`/games`, {
					ID: vm.state.ID,
					coverImgUrl: vm.state.coverImgUrl,
					Image1: vm.state.Image1,
					Image2: vm.state.Image2,
					Image3: vm.state.Image3
				}).then(
						res => {
							vm.state.newGame = res;
							vm.state.ID = '';
							vm.state.coverImgUrl = '';
							vm.state.Image1 = '';
							vm.state.Image2 = '';
							vm.state.Image3 = '';
						},
						err => {
							toasterSvc.error(err, `An error occurred`);
						}
				);
			}
		};
	};

	let template = `
		<div class="admin__container">
			<main-nav background="true?'light':''"></main-nav>
			<div class="container">
				<div style="width: 400px;">
					<h1>Add Game <small>Admin</small></h1>
					<div class="alert mb-lg" ng-show="adminAddGame.state.newGame">
						Successfully created <a ui-sref="game({gameUri: adminAddGame.state.newGame.uri})">
							{{adminAddGame.state.newGame.title}}.
						</a>
					</div>
					<div class="form-group">
						<label>Giant Bomb Id</label>
						<input type="text" class="form-control"
									 ng-model="adminAddGame.state.ID">
					</div>
					<div class="form-group">
						<label>Cover Image Url</label>
						<input type="text" class="form-control"
									 ng-model="adminAddGame.state.coverImgUrl">
					</div>
					<div class="form-group">
						<label>Image 1</label>
						<input type="text" class="form-control"
									 ng-model="adminAddGame.state.Image1">
					</div>
					<div class="form-group">
						<label>Image 2</label>
						<input type="text" class="form-control"
									 ng-model="adminAddGame.state.Image2">
					</div>
					<div class="form-group">
						<label>Image 3</label>
						<input type="text" class="form-control"
									 ng-model="adminAddGame.state.Image3">
					</div>
					<div class="text-right">
						<button class="btn btn-primary" ng-click="adminAddGame.submit()">Add</button>
					</div>
				</div>
			</div>
		</div>
	`;

	return {
		restrict: 'E',
		controller: ctrl,
		controllerAs: 'adminAddGame',
		template: template
	};
};