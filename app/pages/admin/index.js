module.exports = function(app) {
	app.config(require('./config'));
	require('./add-game')(app);
	require('./edit-game')(app);
};