module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
		.state('admin', {
			url: '/admin',
			template: '<ui-view></ui-view>',
			controller: function(authSvc, $state) {
				"ngInject";
				if (!authSvc.isAdmin()) {
					$state.go('home');
				}
			}
		}).state('admin.addGame', {
			url: '/add-game',
			template: `<admin-add-game></admin-add-game>`
		})
		.state('admin.editGame', {
			url: '/edit-game',
			template: `<admin-edit-game></admin-edit-game>`
		});
};