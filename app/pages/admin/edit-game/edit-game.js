import * as api from '../../../core/api';
import state from '../../../core/state';

module.exports = function() {
	let ctrl = function(toasterSvc, $scope, dialogSvc, gameSvc) {
		"ngInject";
		let vm = this;
		vm.state = new state($scope, vm);
		vm.state.set({
			title: '',
			description: '',
			originalReleaseDate: '',
			coverImgUrl: '',
			esrb: '',
			platforms: [],
			genres: [],
			aliases: '',
			error: false,
			images: [],
			videos: [],
			uri: '',
			id: ''
		});
		vm.options = {
			platforms: [],
			genres: [],
			esrbs: []
		};
		api.prod.get('/games/platforms').then(
			res => {
				$scope.$applyAsync(function(){
					vm.options.platforms = res.data.sort();
				});
			}
		);
		api.prod.get('/games/genres').then(
			res => {
				$scope.$applyAsync(function(){
					vm.options.genres = res.data.sort();
				});
			}
		);
		api.prod.get('/games/esrbs').then(
			res => {
				$scope.$applyAsync(function(){
					vm.options.esrbs = res.data.sort();
				});
			}
		);
		let parseAliases = function(aliases){
			if(aliases && aliases.length > 0) {
				return aliases.split(',');
			}

			return [];
		};
		vm.submit = () => {
			vm.state.set({ error: '' });
			if (vm.state.title && vm.state.description && vm.state.originalReleaseDate && vm.state.coverImgUrl && vm.state.esrb) {
				api.prod.post(vm.state.id ? '/games/edit/' + vm.state.id : '/games/create', {
					title: vm.state.title,
					description: vm.state.description,
					originalReleaseDate: vm.state.originalReleaseDate,
					coverImgUrl: vm.state.coverImgUrl,
					esrb: vm.state.esrb,
					aliases: parseAliases(vm.state.aliases),
					genres: vm.state.genres,
					platforms: vm.state.platforms,
					images: vm.state.images,
					videos: vm.state.videos,
					uri: vm.state.uri
				}).then(
					res => {
						vm.state.set({
							title: '',
							description: '',
							originalReleaseDate: '',
							coverImgUrl: '',
							esrb: '',
							error: '',
							aliases: '',
							platforms: [],
							genres: [],
							images: [],
							videos: [],
							uri: '',
							id: ''
						});
						toasterSvc.info('Game Saved Successfully');
					},
					err => {
						vm.state.set({error: JSON.stringify(err, null, 2)});
						toasterSvc.error(err, 'An error occurred');
					}
				);
			}
			else{
				vm.state.set({error: 'All fields are required'});
			}
		};

		vm.getGameData = () => {
			if(vm.state.uri){
				api.prod.get('/games/' + vm.state.uri).then(
					res => {
						$scope.$applyAsync(function(){
							vm.state.set({
								title: res.data.title,
								description: res.data.description,
								originalReleaseDate: new Date(res.data.originalReleaseDate),
								coverImgUrl: res.data.coverImgUrl,
								esrb: res.data.esrb,
								platforms: res.data.platforms.map(function(platform){
									return platform.name;
								}),
								genres: res.data.genres,
								aliases: res.data.aliases.toString(),
								images: res.data.images,
								videos: res.data.videos,
								uri: res.data.uri,
								id: res.data._id,
								error: ''
							});
						});
					},
					err => {
						vm.state.set({error: 'Unable to find a game with a URI of \"' + vm.state.uri + '\"'});
					}
				);
			}
			else{
				vm.state.set({error: 'Game URI is required'});
			}
		};

		vm.deleteGame = () => {
			if(vm.state.uri){
				dialogSvc.open(
					'Delete Game',
					'Are you sure you want to delete this game? This action cannot be undone.',
					() => {
						gameSvc.deleteGame(vm.state.uri).then(
							res => {
								toasterSvc.info('Game Deleted Successfully');
							},
							error => {
								toasterSvc.warning('Error Deleting Game');
							}
						);
					}
				)
			}
			else{
				vm.state.set({error: 'Game URI is required'});
			}
		};

	};

	return {
		restrict: 'E',
		controller: ctrl,
		controllerAs: 'adminEditGame',
		template: require("./edit-game.html")
	};
};