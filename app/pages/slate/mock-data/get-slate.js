module.exports = {
  "id": "4a772b6d-ba26-4833-b0c8-101837ed254b",
  "name": "Batman's Last Ride",
  "uri": "batmans-last-ride",
  "author": {
    "id": 123,
    "name": "Tony Lefler",
    "handle": "@geoctrl",
    "profileImgUrl": null
  },
  "lead": "Can Batman's last game live up to the hype?",
	"preview": "Lorem ipsum dolor sit amet, brute harum pro ad. Oblique quaestio in eam, quo solum docendi id. Eu omnesque posidonium pri. Agam animal duo id, contentiones intellegebat consequuntur per ei. Paulo dictas copiosae eu cum. Ut eloquentiam definitionem sit, at movet mucius denique vix.",
  "coverImgUrl": "http://gearnuke.com/wp-content/uploads/2015/06/arkhamknight3.jpg",
	"body": "Lorem ipsum dolor sit amet, brute harum pro ad. Oblique quaestio in eam, quo solum docendi id. Eu omnesque posidonium pri. Agam animal duo id, contentiones intellegebat consequuntur per ei. Paulo dictas copiosae eu cum. Ut eloquentiam definitionem sit, at movet mucius denique vix. His verterem accusamus scribentur et. Tempor concludaturque at mei, at vim tritani verterem. Vix ne case diceret, ei duo ancillae elaboraret. Vel ad erat maluisset vituperatoribus, copiosae complectitur ea vel. Usu esse alterum assueverit ex. Dicta molestiae usu et, legendos volutpat cu qui. Aliquam principes sit no.Fabellas electram pertinacia ut ius, brute salutandi expetendis ex vel. Pri ad cetero oporteat. Vis ea iriure forensibus dissentias, cu mei discere ornatus. Cum mutat idque percipit te, wisi animal ei usu. Mel stet malis ut, ad est quot graeco, eum eu clita legimus facilis.Pri iriure epicurei oporteat ex, pro ea ludus tempor appellantur. Ad eam mundi malorum appareat, duo cu iisque veritus, et homero laboramus nam. Te commune scaevola fabellas quo, nonumes persequeris mea ad. Eos ea odio quaestio reformidans, corpora efficiendi sed eu. Inani causae ex vim, eros legere tempor vim ex, nonumy vivendo adipisci cum ne. An eos erant apeirian, has ut tempor delicata, mea stet mediocrem in. His ei sumo quidam ponderum. Ad alia moderatius repudiandae eam, tritani torquatos pro ex. Ad simul viris vituperatoribus pro, mea summo interesset et.",
  "tags": [
	  {
		  id: 1234,
		  type: 'media',
		  key: 'game',
		  name: 'Game',
		  abbr: null,
		  val: 'Batman: Arkham Knight'
	  },
	  {
		  id: 1235,
		  type: 'category',
		  key: 'gameplay',
		  name: 'Gameplay',
		  abbr: null,
		  val: 'boring'
	  },
	  {
		  id: 1236,
		  type: 'category',
		  key: 'graphics',
		  name: 'Graphics',
		  abbr: null,
		  val: 'Awesome'
	  },
	  {
		  id: 1235,
		  type: 'genre',
		  key: 'open-world',
		  name: 'Open World',
		  abbr: null,
		  val: 'In-Depth'
	  },
	  {
		  id: 1237,
		  type: null,
		  key: null,
		  name: null,
		  abbr: null,
		  val: 'Crazy Cool Game'
	  }
  ],
  "published": 1454205282709,
  "viewed": 553,
  "rating": {
    "total": 95,
    "up": 110,
    "down": 15
  },
  "game": {
    "id": "682657fa-88e2-4f6e-893d-610299ece34e",
    "title": "Batman: Arkham Knight",
    "uri": "batman-arkham-knight"
  }
};