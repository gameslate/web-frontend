module.exports = function($stateProvider) {
	"ngInject";
	$stateProvider
		.state('edit', {
			url: '/edit/:id/:type',
			controller: function($scope, edit) {
				"ngInject";
				$scope.edit = edit;
			},
			template: '<edit data="edit"></edit>',
			resolve: {
				edit: function($stateParams, toasterSvc, $state, $q, slateSvc) {
					"ngInject";
					return $q((resolve, reject) => {
						slateSvc.getDraft($stateParams.id).then(
							res => {
								resolve(res);
							},
							error => {
								$state.go('home');
								toasterSvc.warning(`Unable to resolve that URL.`, `404`);
							}
						)
					});
				}
			}
		})
		.state('slate', {
			url: '/slate/:username/:slateUri',
			controller: function($scope, slate) {
				"ngInject";
				$scope.slate = slate;
			},
			template: '<view-slate data="slate"></view-slate>',
			resolve: {
				slate: function($stateParams, $state, $q, slateSvc, toasterSvc) {
					"ngInject";

					return $q((resolve, reject) => {
						if ($stateParams.slateUri) {
							slateSvc.getSlate($stateParams.slateUri, $stateParams.username).then(
									res => {
										resolve(res);
									},
									error => {
										reject();
										toasterSvc.warning(`We couldn't find the requested Slate.`, `Whoops`);
									}
							);
						} else {
							reject();
							toasterSvc.warning(`We couldn't find the requested Slate.`, `Whoops`);
						}
					});
				}
			}
		});
};