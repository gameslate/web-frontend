let _debounce = require('lodash/debounce');

module.exports = function () {
	let editCtrl = function($scope, $state, headerSvc, $stateParams, slateSvc, $filter, userSvc, dialogSvc, loadSvc, toasterSvc, authSvc, cloudinarySvc) {
		"ngInject";

		let vm = this;
		vm.data = vm.bindData;
		vm.me = authSvc.getUser();
		vm.editorOptions = {
			content: {
				placeholder: {text: `Write your Slate Here...`}
			}
		};

		headerSvc.set({
			title: vm.data.title?vm.data.title:'Untitled'
		});

		vm.deleteSlate = () => {
			dialogSvc.open(
				'Delete Slate',
				'Are you sure you want to delete this slate? This action cannot be undone.',
				() => {
					$state.go('game', {gameUri: vm.data.game.uri});
					slateSvc.deleteSlate(vm.data.id).then(
						res => {
							toasterSvc.info('Slate Deleted Successfully');
						},
						error => {

						}
					);
				})
		};

		vm.save = _debounce(function() {
			loadSvc.start();
			if ($stateParams.type == 'draft') {
				headerSvc.set({
					title: vm.data.title?vm.data.title:'Untitled'
				});
				slateSvc.saveDraft(vm.data).then(
						res => {
							loadSvc.end();
						},
						error => {
							loadSvc.end();
						}
				);
			}
		}, 500);

		let validate = () => {
			return vm.data.title && vm.data.body;
		};

		vm.publish = function() {
			if (validate()) {
				slateSvc.publishDraft(vm.data._id).then(
						res => {
							$state.go('slate', {slateUri: res.data.uri, username: vm.me.username}, {
								location: 'replace'
							});
							toasterSvc.success(`Successfully published your slate!`, `Awesome`);
						},
						error => {
							$scope.$applyAsync(() => {
								toasterSvc.error('Unable to publish draft', 'Error');
							});
							console.error(error);
						}
				);
			} else {
				$scope.$applyAsync(() => {
					toasterSvc.warning(`The title and body of the slate are required`, `We can't publish the slate yet`);
				});
			}
		};

		// on input change, this function is called
		vm.selectFile = function(file) {
			loadSvc.start();
			cloudinarySvc.upload(file, vm.data.game._id).then(
				res => {
					$scope.$applyAsync(() => {
						vm.data.coverImgUrl = $filter('scaleImage')(res, 1920);
						vm.save();
					});
					loadSvc.end();
				},
				error => {
					loadSvc.end();
					console.error(error);
				}
			);
		};

		vm.removeCoverImg = () => {
			$scope.$applyAsync(() => {
				vm.data.coverImgUrl = null;
				vm.save();
			});
		};



	};

	return {
		restrict: 'E',
		scope: {
			bindData: '=data'
		},
		controller: editCtrl,
		controllerAs: 'edit',
		bindToController: true,
		template: require('./edit.html')
	};
};