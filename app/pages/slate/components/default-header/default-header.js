gameApp.directive('defaultHeader', function() {

	let defaultHeaderCtrl = function($scope, $element) {

	};

	return {
		restrict: 'A',
		scope: {
			coverImgUrl: '='
		},
		controller: defaultHeaderCtrl
	};
});