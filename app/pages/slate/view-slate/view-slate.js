
module.exports = function() {
	let viewSlateCtrl = function(headerSvc, $filter) {
		"ngInject";

		let vm = this;
		vm.data = vm.bindData;
		if (vm.data.coverImgUrl) {
			vm.data.coverImgUrl = $filter('scaleImage')(vm.data.coverImgUrl, 1920);
		}

		headerSvc.set({
			title: vm.data.title,
			description: vm.data.subtitle,
			image: vm.data.coverImgUrl || undefined,
			type: 'article'
		});

	};

	return {
		restrict: 'E',
		scope: {
			bindData: '=data'
		},
		controller: viewSlateCtrl,
		controllerAs: 'viewSlate',
		bindToController: true,
		template: require('./view-slate.html')
	};
};