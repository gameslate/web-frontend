module.exports = function(app) {
	app.config(require('./config'));
	app.directive('edit', require('./edit/edit'));
	app.directive('viewSlate', require('./view-slate/view-slate'));
};