import signIn from './sign-in';
import register from './register';

module.exports = function(app) {
	app.directive('signIn', signIn);
	app.directive('register', register);
	
	require('./components')(app);
};