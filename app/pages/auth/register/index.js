import _debounce from 'lodash/debounce';
import state from '../../../core/state';

function cleanUsername(username) {
	return username?!(username.search(/^[a-zA-Z0-9-_]+$/) == -1):true;
}

export default function() {
	let ctrl = function($scope, tsModalService, authSvc) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, vm);
		vm.state.set({
			component: 'form', // form | loading
			emailError: false,
			usernameError: false,
			emailThumb: false,
			usernameThumb: false,
			emailLoading: false,
			usernameLoading: false
		});

		vm.form = {};

		vm.checkEmail = _debounce(email => {
			if (email && $scope.registerForm.email.$valid) {
				vm.state.set({emailLoading: true});
				authSvc.isEmailUnique(email).then(
						res => {
							vm.state.set({
								emailError: false,
								emailThumb: true
							});
						},
						error => {
							vm.state.set({
								emailError: 'Email is taken',
								emailThumb: false
							});
						}
				).finally(() => {
					vm.state.set({emailLoading: false});
				});
			}
		}, 300);

		vm.lowerCase = username => {
			if (username) {
				vm.form.username = vm.form.username.toLowerCase();
			}
		};

		vm.checkUsername = _debounce(username => {
			if (username) {
				username = username.toLowerCase();
			}
			if (cleanUsername(username)) {
				if (username && username.length >= 3) {
					vm.state.set({usernameLoading: true});
					authSvc.isUsernameUnique(username).then(
							res => {
								vm.state.set({
									usernameError: false,
									usernameThumb: true
								});
							},
							error => {
								vm.state.set({
									usernameError: `@${username} is taken`,
									usernameThumb: false
								});
							}
					).finally(() => {
						vm.state.set({usernameLoading: false});
					});
				}
			} else {
				vm.state.set({
					usernameError: `Only alphanumeric characters, dashes ( - ) and underscores ( _ ) are allowed`,
				});

			}
		}, 300);

		vm.submit = () => {
			authSvc.register({
				username: vm.form.username,
				email: vm.form.email,
				password: vm.form.password,
				'g-recaptcha-response': vm.form.captcha
			}).then(
					res => {
						tsModalService.submit();
					}
			);
		};

		vm.close = () => {
			tsModalService.cancel('cancel');
		};
	};

	return {
		restrict: 'E',
		scope: {
			alert: '='
		},
		controller: ctrl,
		controllerAs: 'register',
		bindToController: true,
		template: require('./register.html')
	};
};