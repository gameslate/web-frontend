import state from '../../../core/state';

export default function() {

	let ctrl = function($scope, tsModalService, authSvc) {
		"ngInject";
		
		let vm = this;

		vm.state = new state($scope, vm);
		vm.state.set({
			component: 'form', // form | loading
			submitFailed: false
		});
		
		vm.signInFailedMessage = 'Sign In failed - please try again';
		vm.signInForm = {};
		vm.signInFields = [
			{
				key: 'username',
				type: 'prefixInput',
				templateOptions: {
					label: 'Gamer Handle',
					type: 'text',
					placeholder: 'gamer_handle',
					required: true,
					minlength: 3,
					prefix: '@'
				}
			},
			{
				key: 'password',
				type: 'input',
				templateOptions: {
					type: 'password',
					label: 'Password',
					placeholder: 'Password',
					required: true
				}
			}
		];

		vm.register = () => {
			authSvc.registerModal();
			vm.close();
		};


		vm.submit = () => {
			vm.state.set({component: 'loading'});
			if (vm.signInValidate.$valid) {
				authSvc.authenticate(
						vm.signInForm.username,
						vm.signInForm.password
				).then(
						res => {
							tsModalService.submit();
						},
						error => {
							vm.state.set({component: 'form'});
							if (error.status == 401) {
								vm.state.set({
									submitFailed: true
								});
							} else {
								console.error(error)
							}
						}
				);
			}
		};

		vm.close = () => {
			tsModalService.cancel();
		};
	};

	return {
		restrict: 'E',
		scope: {
			alert: '='
		},
		controller: ctrl,
		controllerAs: 'signIn',
		bindToController: true,
		template: require('./sign-in.html')
	}
}