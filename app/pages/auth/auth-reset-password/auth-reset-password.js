module.exports = function($stateProvider) {
	$stateProvider
			.state('authResetPassword', {
				url: '/reset-password',
				templateUrl: 'auth-reset-password'
			})
};