import _isBoolean from 'lodash/isBoolean';

module.exports = function() {
	let ctrl = function($scope, $element) {
		"ngInject";

		if (__DEV__) {
			let grecaptcha =  {
						render: (el, obj) => {}
					};
		}

		let cb = res => {
			$scope.$applyAsync(() => {
				$scope.ngModel = res;
			});
		};

		let id = grecaptcha.render(
				$element[0],
				{
					'sitekey':'6LeYOB0TAAAAAP9KZCPxv9hh_fDxqw_qWB7APvR6',
					'callback': cb,
					'expired-callback': cb
				}
		);
	};

	let template = `<div class="grecaptcha"></div>`;

	return {
		restrict: 'E',
		replace: true,
		scope: {
			ngModel: '='
		},
		controller: ctrl,
		template: template
	}
};