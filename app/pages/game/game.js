import state from '../../core/state';

module.exports = function() {
	let gameCtrl = function($scope, headerSvc, $stateParams, gameSvc, slateSvc, userSvc, authSvc, shareSvc, loadSvc, $state, dialogSvc, toasterSvc) {
		"ngInject";
		let vm = this,
				userScoredGame = false;

		vm.state = new state($scope, this);
		vm.state.set({
			similarGames: [],
			popularSlates: 'noData', // loading | noData | hasData | error
			freshSlates: 'noData' // loading | noData | hasData | error
		});
		vm.data = vm.dataBind;
		vm.gameHasReleased = new Date(vm.data.originalReleaseDate)<Date.now();
		vm.me = userSvc.get('me');
		vm.userIsAuthenticated = authSvc.isAuthenticated();
		vm.userIsAdmin = authSvc.isAdmin();
		vm.similarGames = [];

		headerSvc.set({
			title: vm.data.title,
			image: vm.data.coverImgUrl,
			description: vm.data.description
		});

		let hasUserScored = () => {
			if (vm.me) {
				gameSvc.getStats(vm.data.uri, vm.me._id).then(
						res => {
							if (res) {
								userScoredGame = true;
							}
						}
				);
			}
		};
		
		$scope.$on('userScored', function() {
			hasUserScored();
		});

		hasUserScored();

		vm.writeSlate = () => {
			if (authSvc.isAuthenticated() && userScoredGame) {
				loadSvc.start();
				slateSvc.createDraft(vm.data._id).then(
						res => {
							loadSvc.end();
							$state.go('edit', {id: res._id, type: 'draft'});
						},
						error => {
							loadSvc.end();
						}
				);
			} else {
				if (!authSvc.isAuthenticated()) {
					authSvc.signInModal({
						title: 'Whoops',
						message: 'You must be signed in to write a slate.'
					});
				} else if (!userScoredGame) {
					dialogSvc.open(`Whoops...`, `You need to score this game before writing a slate.`, `Score Game`, `Nevermind`)
							.then(
									res => {
										$scope.$broadcast('scoreThisGame', 'write');
									});
				}
			}
		};
		
		vm.share = () => {
			shareSvc.shareGame();
		};

		slateSvc.getGameSlates(vm.data._id, ['-publishedDate']).then(
				res => {
					vm.freshSlates = res.data.docs;
					if (res.data.docs.length) {
						vm.state.set({freshSlates: 'hasData'});
					}
				}
		);
		
		slateSvc.getGameSlates(vm.data._id, ['upVotes']).then(
				res => {
					vm.popularSlates = res.data.docs;
					if (res.data.docs.length) {
						vm.state.set({popularSlates: 'hasData'});
					}
				}
		);

		gameSvc.getSimilar(vm.data.uri).then(
				res => {
					vm.state.set({ similarGames: res });
				}
		);
	};

	return {
		scope: {
			dataBind: '=data'
		},
		template: require('./game.html'),
		controllerAs: 'game',
		bindToController: true,
		controller: gameCtrl
	};
};