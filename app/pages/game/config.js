module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
		.state('game', {
			url: '/game/:gameUri',
			template: `<game data="gameData"></game>`,
			controller: function($scope, gameData) {
				"ngInject";
				$scope.gameData = gameData;
			},
			resolve: {
				gameData: function($stateParams, $state, $q, gameSvc, toasterSvc) {
					"ngInject";

					return $q((resolve, reject) => {
						gameSvc.getGame($stateParams.gameUri).then(
								res => {
									resolve(res);
								},
								error => {
									reject();
									$state.go('home');
									toasterSvc.warning(`Couldn't find the Requested Page`, `404`);
								});
					});

				}
			}
		});
};