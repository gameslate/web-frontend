module.exports = function(app) {

	let config = require('./config');
	let game = require('./game');

	app.config(config);
	app.directive('game', game);

};