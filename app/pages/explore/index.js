module.exports = function(app) {
	let config = require('./config');
	let search = require('./explore');
	let searchInput = require('./search-input');
	let searchResults = require('./search-results');

	app.config(config);
	app.directive('explore', search);
	app.directive('searchInput', searchInput);
	app.directive('searchResults', searchResults);
};