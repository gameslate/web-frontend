import _debounce from 'lodash/debounce';
import state from '../../core/state';

module.exports = function() {
	var searchInputCtrl = function($scope, $element, searchSvc, $document) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, vm);
		vm.state = {

		};

		let stateSearch = searchSvc.getStates();
		stateSearch.on('close', () => {
			$element.removeClass('isFocus');
		});
		stateSearch.on('open', () => {
			$element.addClass('isFocus');
		});

		vm.inputHandler = _debounce(() => {
			search();
		}, 100);

		let searchInputEl = angular.element($element[0].children[0]);
		let searchIconEl = angular.element($element[0].children[1]);

		searchInputEl.on('focus, click', e => {
			e.stopPropagation();
			stateSearch.goTo('open');
		});

		let search = () => {
			if (vm.state.term.length > 0) {
				searchSvc.search(vm.state.term);
			} else {
			}
		};

		$document.on('click', () => {
			stateSearch.goTo('close');
		});

	};

	let template = `
		<input type="search"
				class="form-control search__input"
				placeholder="Search"
				ng-model="searchInput.state.term"
				ng-change="searchInput.inputHandler()">
		<div class="search__input-icon">
			<icon icon="search" class="search__input-icon"></icon>
		</div>
		<search-results></search-results>
	`;

	return {
		restrict: 'E',
		controller: searchInputCtrl,
		controllerAs: 'searchInput',
		bindAsController: true,
		template: template
	}
};
