import state from '../../core/state';

module.exports = function() {

	let searchResultsCtrl = function($scope, searchSvc, $state, $document) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, this);
		vm.state.set({
			open: false,
			data: [],
			input: null,
			selected: null,
			component: 'start' // start | loading | hasData | noData | error
		});

		vm.clickHandler = e => {
			e.stopPropagation();
		};

		// $document.bind('keypress', function() {
		// 	console.log('here')
		// });

		vm.handleGo = (type, param) => {
			$state.go(type, param);
		};

		let stateSearch = searchSvc.getStates();
		stateSearch.on('close', () => {vm.state.set({open: false})});
		stateSearch.on('open', () => {vm.state.set({open: true})});

		$scope.$watch(() => {
			return vm.term;
		}, newVal => {
			vm.state.set({
				input: newVal
			})
		});

		searchSvc.passResultsScope($scope);
		$scope.updateData = (term, data) => {
			vm.state.set({
				component: data.games.length > 0 || data.reviews.length > 0?'hasData':'noData',
				input: term,
				data: data
			});
		};

	};

	let template = `
		<div class="search-results" ng-click="clickHandler()">

			<div class="p-lg pt-none" ng-show="searchResults.state.component != 'start' && (!searchResults.state.data.games.length && !searchResults.state.data.reviews.length)">
				No Results
			</div>

			<div class="list__header" ng-show="searchResults.state.data.reviews.length">
				Slates
			</div>
			<ul class="list-select">
				<li ng-repeat="item in searchResults.state.data.reviews | limitTo: 5">
					<a ui-sref="slate({slateUri: item.uri, username: item.author.username})">{{ item.title }}</a>
				</li>
			</ul>
			<div class="list__header" ng-show="searchResults.state.data.games.length">
				Games
			</div>
			<ul class="list-select">
				<li ng-repeat="item in searchResults.state.data.games | limitTo: 5">
					<a ng-click="searchResults.handleGo('game', {gameUri: item.uri})">{{ item.title }}</a>
				</li>
				<li class="divider"
						ng-show="searchResults.state.component == 'noData' || searchResults.state.component == 'hasData'"></li>
				<li ng-show="searchResults.state.component == 'noData'">
					<a ui-sref="explore({search: searchResults.state.input})">No luck? Try our <strong>Advanced Search</strong> on the explore page</a>
				</li>
				<li ng-show="searchResults.state.component == 'hasData'">
					<a ui-sref="explore({search: searchResults.state.input})">
						See more results on the <strong>Explore</strong> page
					</a>
				</li>
			</ul>

			<div class="p-lg pt-none" ng-show="searchResults.state.component == 'start'">
				Type to start Searching...
			</div>

			<div ng-show="searchResults.state.component == 'loading'"
					 style="height: 80px; position: relative;">
				<div class="load"><div class="load"></div></div>
			</div>
		</div>
	`;

	return {
		restrict: 'E',
		scope: {
		},
		controller: searchResultsCtrl,
		controllerAs: 'searchResults',
		template: template
	};
};
