module.exports = function($stateProvider) {
	"ngInject";

	$stateProvider
			.state('explore', {
				url: '/explore',
				template: `<explore></explore>`,
				params: {
					search: null
				}
			});
};