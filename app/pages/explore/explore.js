import _debounce from 'lodash/debounce';
import state from '../../core/state';

module.exports = function() {
	let searchCtrl = function($scope, $stateParams, headerSvc, searchSvc, localStorageService) {
		"ngInject";

		headerSvc.set({
			title: 'Explore'
		});

		if ($stateParams.search) {
			localStorageService.set('search.term', $stateParams.search);
		}

		function searchIt() {
			if (vm.state.term.length >= 1) {
				// save last searched term
				localStorageService.set('search.term', vm.state.term);
				vm.state.set({status: 'loading'});
				searchSvc.search(vm.state.term).then(
						res => {
							vm.state.set({
								status: 'hasData',
								data: res
							});
						},
						error => {
							vm.state.set({status: 'error'});
						}
				);
			} else {
				localStorageService.set('search.term', null);
				vm.state.set({status: 'noTerm'});
			}
		}

		let vm = this;
		vm.state = new state($scope, vm);

		vm.state.set({
			status: 'noTerm', // noTerm | loading | hasData | noResults | error
			data: [],
			term: localStorageService.get('search.term')?localStorageService.get('search.term'):''
		}, searchIt);

		vm.searchDebounce = _debounce(searchIt, 300);
	};


	let template = `
		<div class="search__container">
			<div class="search__top">
				<main-nav background="true?'light':''"></main-nav>
			</div>
			<div class="search__results">
				<div class="container-slate">
					<div class="search__box">
						<input type="text" placeholder="Search"
								   ng-model="search.state.term"
								   ng-change="search.searchDebounce()"
								   class="form-control">
						<div class="search__box-icon">
							<icon icon="search"></icon>
						</div>
					</div>
					<div class="load mt-lg" style="height: 100px; position:relative;" ng-show="search.state.status == 'loading'">
						<div class="load"></div>
					</div>
					<div ng-show="search.state.status == 'noTerm'" ng-if="false">
						<div class="search__tabs tabs" tabs>
							<button class="tabs-btn" tabs-item="trending">
								Trending
							</button>
							<button class="tabs-btn" tabs-item="recommended">
								Recommended
							</button>
							<div class="tabs-line"></div>
							<div tabs-content="trending">
								trending slates
							</div>
							<div tabs-content="recommended">
								recommended
							</div>
						</div>
					</div>
					<div ng-show="search.state.status == 'hasData'">
						<div class="search__tabs tabs" tabs>
							<button class="tabs-btn" tabs-item="search-game">
								Games ({{search.state.data.games.length}})
							</button>
							<button class="tabs-btn" tabs-item="search-slate">
								Slates ({{search.state.data.reviews.length}})
							</button>
							<div class="tabs-line"></div>
							<div tabs-content="search-slate" class="pt-lg">
								<s-grid>
								<slate-box slate="slate" ng-repeat="slate in search.state.data.reviews"></slate-box>								
								</s-grid>
							</div>
							<div tabs-content="search-game">
								<table class="search-list">
									<tr ng-repeat="game in search.state.data.games" ui-sref="game({gameUri: game.uri})">
										<td class="search-list__img">
											<div style="background-image: url({{game.coverImgUrl | scaleImage: 650}})">
												<div class="search-list__img-none" ng-show="!game.coverImgUrl">
													<icon icon="image"></icon>
												</div>
											</div>
										</td>
										<td class="search-list__title">
											{{game.title}}
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	`;

	return {
		restrict: 'E',
		scope: {},
		controller: searchCtrl,
		controllerAs: 'search',
		bindToController: true,
		template: template
	};
};