module.exports = function(app) {
	require('./home')(app);
	require('./game')(app);
	require('./slate')(app);
	require('./explore')(app);
	require('./auth')(app);
	require('./profile')(app);
	require('./about')(app);
	require('./admin')(app);
//	require('./feedback')(app);
};