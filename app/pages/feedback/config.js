module.exports = function($stateProvider) {
	"ngInject";
	
	$stateProvider
			.state('home', {
				url: '/',
				template: '<home data="homeSlider"></home>',
				controller: function($scope, homeSlider) {
					"ngInject";
					$scope.homeSlider = homeSlider;
				},
				resolve: {
					homeSlider: function() {
						"ngInject";
						return {hey: 'hey'}
						//return slateSvc.getTopSlates();
					}
				}
			})
};
