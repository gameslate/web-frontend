import state from '../../core/state';

module.exports = function() {
	var homeCtrl = function($scope, headerSvc, slateSvc, gameSvc, authSvc) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, this);
		vm.state.set({
			trendingSlates: null,
			freshSlates: null,
			newGames: null,
			loggedIn: authSvc.isAuthenticated()
		});

		vm.register = () => {
			authSvc.registerModal();
		};

		headerSvc.set();

		slateSvc.getSlates(['-upVotes']).then(
				res => {
					vm.state.set({
						trendingSlates: res.data.docs
					})
				},
				err => {
					console.error('err', err);
				}
		);

		slateSvc.getSlates(['-publishedDate']).then(
				res => {
					vm.state.set({
						freshSlates: res.data.docs
					})
				}
		);

		gameSvc.getGames({limit: 22}).then(
				res => {
					vm.state.set({
						newGames: res.data.docs
					})
				}
		);

		
	};

	return {
		restrict: 'E',
		controller: homeCtrl,
		controllerAs: 'home',
		template: require('./home.html')
	};
};