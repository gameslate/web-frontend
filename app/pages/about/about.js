module.exports = function() {
	return {
		restrict: 'E',
		template: require('./about.html'),
		controller: function(headerSvc) {
			"ngInject";
			headerSvc.set({
				title: 'About'
			});
		}
	};
};