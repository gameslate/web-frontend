module.exports = function(app) {
	app.config(require('./config'));
	app.directive('about', require('./about'));
};