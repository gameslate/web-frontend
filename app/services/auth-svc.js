import * as api from '../core/api';
import _isNull from 'lodash/isNull';
import _forEach from 'lodash/forEach';
import moment from 'moment';

module.exports = function(localStorageService, tsModalService, $state, $q, toasterSvc) {
	"ngInject";

	let authToken = localStorageService.get('auth.token');
	let user = localStorageService.get('auth.user');

	function open(type, alert) {
		tsModalService.open({
			directive: type,
			size: 'small',
			closeBackdrop: false,
			resolve: {
				alert: alert ? alert : false
			}
		});
	}
	
	return {
		isAuthenticated() {
			return !!localStorageService.get('auth.token');
		},

		isTokenExpired() {
			return localStorageService.get('auth.expires') <= Date.now();
		},

		authenticate(username, password) {
			return api.prod.post('/authenticate', { username, password }).then(
				res => {
					user = res.data.user;
					authToken = res.data.token;
					localStorageService.set('auth.token', res.data.token);
					localStorageService.set('auth.expires', (res.data.expires * 1000) + Date.now());
					localStorageService.set('auth.user', res.data.user);
					toasterSvc.info('Signed In successfully');
					$state.go($state.current.name, {}, {reload: true});
					return res.data;
				}
			);
		},

		getAuthToken() {
			return authToken;
		},
		
		getTokenExpire() {
			return localStorageService.get('auth.expires');
		},

		isAdmin() {
			return user && user.role == 'admin';
		},

		getUser() {
			return user;
		},

		register(data) {
			return api.prod.post('/users', data).then(
					res => {
						toasterSvc.success(`Registered Successfully`);
						this.authenticate(data.username, data.password);
						return res.data;
					},
					error => {
						console.error(error);
						return error;
					}
			);
		},

		signOut(reason) {
			if (this.isAuthenticated()) {

				let fin = function() {
					if (reason == 'token') {
						toasterSvc.warning('Please sign back in.', 'Your token expired')
					} else {
						toasterSvc.info('Signed out successfully')
					}
					$state.go($state.current.name, {}, {reload: true});
					localStorageService.clearAll();
				};

				return api.prod.get('/logout').then(fin, fin)
			}
		},

		isUsernameUnique(username) {
			return $q((resolve, reject) => {
				api.prod.get(`/users/isAvailable/username?username=${username}`).then(
						res => {
							if (res.data.isAvailable) {
								resolve();
							} else {
								reject();
							}
						},
						error => {
							reject(error);
						}
				);
			});
		},

		isEmailUnique(email) {
			return $q((resolve, reject) => {
				api.prod.get(`/users/isAvailable/email?email=${email}`).then(
						res => {
							if (res.data.isAvailable) {
								resolve();
							} else {
								reject();
							}
						},
						error => {
							reject(error);
						}
				);
			});
		},

		signInModal(alert) {
			open('signIn', alert);
		},

		registerModal() {
			open('register');
		},

		forgotPasswordModal() {
			open('forgotPassword');
		},

		isGameInWishlist(gameId) {
			let isGameInWishlist = false;
			_forEach(user.wishlist, item => {
				if (item._id == gameId) {
					isGameInWishlist = true;
				}
			});
			return isGameInWishlist;
		},

		addWishlist(game) {
			return api.prod.post(`/users/${user.username}/wishlist/${game._id}`).then(
					res => {
						user.wishlist.push({
							_id: game._id,
							coverImgUrl: game.coverImgUrl,
							title: game.title,
							uri: game.uri
						});
						localStorageService.set('auth.user', user);
						return res.data;
					},
					error => {
						return error;
					}
			);
		},

		removeWishlist(gameId) {
			return api.prod.delete(`/users/${user.username}/wishlist/${gameId}`).then(
					res => {
						user.wishlist = user.wishlist.filter(item => {
							return item._id != gameId;
						});
						localStorageService.set('auth.user', user);
						return res.data;
					}
			);
		}
	};
};
