module.exports = function(app) {
	app.service('authSvc', require('./auth-svc'));
	app.service('loadSvc', require('./load-svc'));
	app.service('slateSvc', require('./slate-svc'));
	app.service('userSvc', require('./user-svc'));
	app.service('gameSvc', require('./game-svc'));
	app.service('appUtils', require('./app-utils-svc'));
	app.service('toasterSvc', require('./toaster-svc'));
	app.service('tagSvc', require('./tag-svc'));
	app.service('searchSvc', require('./search-svc'));
	app.service('stateMachineSvc', require('./state-machine-svc'));
	app.service('cloudinarySvc', require('./cloudinary-svc'));
	app.service('headerSvc', require('./header-svc'));
	app.service('statsSvc', require('./stats-svc'));
};
