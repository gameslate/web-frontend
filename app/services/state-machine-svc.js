let _find = require('lodash/find');

module.exports = function() {
	let EventEmitter = require('event-emitter');


	//check each one for a name property, that's all that's required
	function validateStates(arr) {
		return arr.every(item => item.name);
	}

	class StateMachine {

		constructor(statesArr) {
			this.currentState = null;
			this.events = new EventEmitter();
			if (statesArr) {
				if (validateStates(statesArr)) {
					this.setStates(statesArr);
				}
				else {
					throw new Error('StateMachine: all states must have a name');
				}
			}
		}

		/**
		 * set the states and subscribe to each
		 * @param {Array} statesArr
		 */
		setStates(statesArr) {
			this.states = statesArr;
			statesArr.forEach(state => {
				state.visits = 0; //create visits prop to track how many times we've entered the state
			});
			return this;
		}

		/**
		 * start it by transitioning it to the initial state. this method is optional
		 * @return {Object} this
		 */
		start() {
			let initialState = _find(this.states, item => item.initial);
			if (initialState) {
				this.goTo(initialState.name);
			}
			return this;
		}

		/**
		 * check the current state
		 * @param  {String}  stateName
		 * @return {Boolean}
		 */
		isState(stateName) {
			return this.currentState === stateName;
		}

		/**
		 * get a state object
		 * @param  {String} stateName
		 * @return {Object}
		 */
		getState(stateName) {
			return _find(this.states, state => state.name === stateName);
		}

		//Go to specified state
		//alias for EventEmitter.prototype.emit
		goTo(...args) {
			let stateName = args[0]; //state name should always be the first arg
			if (this.isState(stateName)) {
				return this; //don't emit if already in that state
			}
			else {
				let state = this.getState(stateName);
				if (state) {
					++this.getState(stateName).visits;
					this.currentState = stateName;
					this.events.emit.apply(this.events, args); //emit returns `this`
					return this; //return the StateMachine, not EventEmitter
				}
				else {
					throw new Error(`StateMachine: no ${stateName} state.`);
				}
			}
		}

		//another convenient alias.
		on(stateName, fn) {
			this.events.on(stateName, fn);
			return this; //return the StateMachine, not EventEmitter
		}

		/**
		 * subscribe to multiple states with the same callback
		 * if stateNamesArr[0] === '*' -> subscribe to all states
		 * @param  {Array}    stateNamesArr
		 * @param  {Function} fn
		 * @return {Object}   this
		 */
		onStates(stateNamesArr, fn) {
			this.states
				.filter(state => {
					return stateNamesArr[0] === '*' ? true : stateNamesArr.indexOf(state.name) > -1;
				})
				.forEach((state) => {
					this.on(state.name, fn);
				});
			return this;
		}

		/**
		 * add method to each state in this state machine
		 * @param  {String} name of function, will overwrite existing if same name
		 * @param  {Function} function that gets attached to each instance of State
		 * @return {Object} this, for chaining
		 */
		decorateStates(name, fn) {
			this.states.forEach(state => {
				state[name] = fn;
			});
			return this;
		}
	}


	return {
		create(states) {
			return new StateMachine(states);
		}
	};
};