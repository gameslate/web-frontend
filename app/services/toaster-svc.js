let _isNumber = require('lodash/isNumber');
let _isString = require('lodash/isString');
let _isEmpty = require('lodash/isEmpty');
let _isNull = require('lodash/isNull');

module.exports = function($compile, $rootScope, appUtils) {
	"ngInject";

	// create toaster element
	let $scope = $rootScope.$new(),
		toasterContainer = angular.element(
			`<div class="toaster">
					<div class="toaster__el-animate" ng-repeat="toaster in toasterGroup track by $index" ng-show="toaster.show">
						<div class="toaster__el toaster__el-{{toaster.type}}">
							<div class="toaster__title">{{toaster.title}}</div>
							<div class="toaster__content">{{toaster.content}}</div>
							<div class="toaster__close" ng-click="close(toaster.id);">
								<icon icon="close"></icon>
							</div>
						</div>
					</div>
				</div>`),
		defaultOptions = {
			timeout: 7000
		};

	// add toaster container to scope and append to body
	$compile(toasterContainer)($scope);
	angular.element(document.body).append(toasterContainer);

	// create toaster list on scope
	$scope.toasterGroup = [];

	// assign scope.close to close function
	$scope.close = function(toasterId) {
		close(toasterId);
	};

	/**
	 * buildOptions
	 * build options and add user options if available
	 * @param userOptions
	 * @returns {object} options
	 */
	function buildOptions(userOptions) {
		let buildOptions = angular.copy(defaultOptions);
		buildOptions.timeout = typeof userOptions.timeout && (userOptions.timeout===false || _isNumber(userOptions.timeout))?userOptions.timeout:buildOptions.timeout;
		return buildOptions;
	}

	/**
	 * close
	 * removes the specified toaster by id
	 * @param toasterId
	 */
	function close(toasterId) {
		$scope.$evalAsync(function() {
			$scope.toasterGroup = $scope.toasterGroup.filter(function(toast) {
				return toast.id !== toasterId;
			});
		});
	}

	/**
	 * new toaster
	 * creates a new toaster and sets timeout (if specified)
	 * add toaster to scope.toasterGroup
	 * @param content *required
	 * @param title
	 * @param type
	 * @param options
	 * @returns {{id: *, title: *, content: *, type: *, options: Object}}
	 */
	function newToaster(content, title, type, options) {
		var toaster = {
			id: appUtils.guid(),
			title: title,
			content: content,
			type: type,
			show: true,
			options: buildOptions(options)
		};
		if (!_isNull(toaster.options.timeout) && toaster.options.timeout) {
			setTimeout(function() {
				close(toaster.id);
			}, toaster.options.timeout);
		}
		$scope.toasterGroup.push(toaster);
		return toaster;
	}

	/**
	 * set up toaster
	 * @param content required
	 * @param title
	 * @param type
	 * @param options
	 * @returns {object} toaster (false if no content)
	 */
	function setToaster(content, title, type, options) {
		if (!_isEmpty(content) && _isString(content)) {
			return newToaster(content, title, type, options);
		} else {
			console.error(`[toaster error] content argument required`);
			return false;
		}
	}

	return {
		info(content, title=null, options={}) {
			return setToaster(content, title, 'info', options);
		},
		error(content, title=null, options={}) {
			return setToaster(content, title, 'error', options);
		},
		warning(content, title=null, options={}) {
			return setToaster(content, title, 'warning', options);
		},
		success(content, title=null, options={}) {
			return setToaster(content, title, 'success', options);
		}
	}
};