import * as api from '../core/api';

module.exports = function($q) {
	"ngInject";

	let currentGame = {};

	return {
		getGame(gameIdOrUri) {
			return api.prod.get(`/games/${gameIdOrUri}`).then(
					res=> {
						currentGame = res.data;
						return currentGame;
					}
			);
		},
		getCurrentGame() {
			return currentGame;
		},
		getGameSlates(gameId) {

		},
		
		getGames(params) {
			return api.prod.get(`/games`, {
				params: params || {}
			});
		},

		getSimilar(gameUri) {
			return api.prod.get(`/games/${gameUri}/similar`).then(
					res => res.data
			);
		},

		deleteGame(gameUri) {
			return api.prod.delete(`/games/${gameUri}`).then(
				res => res.data
			);
		},

		/**
		 * get game stats
		 * retrieves community stats for a game
		 * @param gameUri
		 * @param userId
		 * @return {object}
		 */
		getStats(gameUri, userId) {
			if (userId) {
				return api.prod.get(`/stats`, {
					params: {
						user: userId,
						gameUri: gameUri
					}
				}).then(
						res => res.data.docs[0]
				);
			} else {
				return api.prod.get(`/games/${gameUri}/stats`).then(
						res => res.data
				);
			}
		},
		getGameTags() {

		}
	}
};