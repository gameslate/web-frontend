import * as api from '../core/api';
import axios from 'axios';

module.exports = function($http, $q, cloudinary, userSvc) {
	"ngInject";

	let cloudinaryApiKey = '975354892123438';

	function getSignature(data) {
		return $q((resolve, reject) => {
			api.prod.get(`/imgSignature`, {
				params: data
			}).then(
					res => {
						resolve(res.data);
					},
					error => {
						reject(error);
					}
			);
		});
	}

	return {
		/**
		 * upload file to cloudinary
		 * @param file
		 * @param imageContext (game, gameCover, slateCover)
		 * @returns {promise} cloudinary promise
		 */
		upload(file, gameId) {
			return $q((resolve, reject) => {
				let me = userSvc.get('me');

				setTimeout(function() {
					let sendData = {
						timestamp: new Date().getTime(),
						public_id: file.name,
						tags: `user__${me._id}, game__${gameId}`
					};

					// get signature before uploading
					getSignature(sendData).then(
							res => {
								sendData.api_key = cloudinaryApiKey;
								sendData.signature = res.key;

								cloudinary.upload(file, sendData)
										.then(
												res => {
													resolve(res.data.url)
												}
										);
							},
							error => {
								reject(error);
							}
					);
				}, 0);
			});
		}
	}
};