module.exports = function($q) {
	'ngInject';
	return {
		getTagTypes() {
			return $q(resolve => {
				resolve(require('../mock-data/get-tag-types'));
			});
		},
		getTags() {
			return $q(resolve => {
				resolve(require('../mock-data/get-tags'));
			});
		}
	};
};