import * as api from '../core/api';

module.exports = function() {
	"ngInject";

	return {
		getStatsByUser(userId) {
			return api.prod.get(`/stats`, {
				params: {
					user: userId
				}
			});
		},
		postStats() {

		},
		putStats(statsId) {

		}
	}
};