module.exports = function($rootScope, $timeout) {
	"ngInject";

	return {
		set(data) {
			$timeout(function() {
				$rootScope.$broadcast('setTitle', data);
			});
		}
	};
};