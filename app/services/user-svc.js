import * as api from '../core/api';

module.exports = function($q, authSvc) {
	"ngInject";
	// todo: set up auth stuff

	return {
		get(userId) {
			if (userId === 'me') {
				return authSvc.getUser();
			} else {
				return api.prod.get(`/users/${userId}`).then(
						res => res.data
				);
			}
		},

		isMe(userId) {
			return this.get('me')._id == userId;
		},

		search(username) {
			api.prod.get(`/users`, {
				params: {
					search: username
				}
			})
		},

		activityFeed(userId) {
			return api.prod.get('/activityFeed', {
				params: {
					user: userId,
					limit: 50
				}
			}).then(res => res.data);
		},

		getSlates(userId) {
			return api.prod.get('/slates', {
				params: {
					userId: userId,
					limit: 50
				}
			}).then(res => res.data);
		}
	};
};