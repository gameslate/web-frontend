import * as api from '../core/api';

module.exports = function($q, stateMachineSvc) {
	"ngInject";

	let stateSearch = new stateMachineSvc.create([
		{name: 'close'},
		{name: 'open'}
	]);
	stateSearch.goTo('close');
	let resultsScope;

	function search(term) {
		return api.prod.get('/search', {
			params: {
				q: term,
				limit: 40
			}
		}).then(res => {
			resultsScope.updateData(term, res.data);
			return res.data
		});
	}

	return {
		search: search,

		getStates() {
			return stateSearch;
		},

		passResultsScope(scope) {
			resultsScope = scope;
		}
	}
};
