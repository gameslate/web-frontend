import * as api from '../core/api';

module.exports = function($q, $http, authSvc) {
	"ngInject";

	return {
		getSlates(sortBy) {
			return api.prod.get(`/slates`, {
				params: {
					sortBy: sortBy,
					limit: 10
				}
			});
		},

		getSlate(slateIdOrUri, username) {
			return api.prod.get(`/slates/${username}/${slateIdOrUri}`).then(
					res => res.data
			);
		},

		saveSlate() {

		},

		getUserSlates(userId) {
			let params = {params:{
				userId: userId?userId:authSvc.getUser().id,
				sortBy: ['-publishedDate']
			}};
			return api.prod.get(`/slates`, params).then(
					res => res.data
			);
		},

		publishDraft(draftId) {
			return api.prod.post(`/slateDrafts/${draftId}/publish`);
		},
		
		createDraft(gameId) {
			return api.prod.post(`/slateDrafts`, {
				game: gameId
			}).then(
					res => res.data
			);
		},

		deleteDraft(draftId) {
			return api.prod.delete(`/slateDrafts/${draftId}`);
		},

		saveDraft(draft) {
			return api.prod.put(`/slateDrafts`, {
				_id: draft._id,
				title: draft.title,
				subtitle: draft.subtitle,
				body: draft.body,
				coverImgUrl: draft.coverImgUrl
			}).then(
					res => res.data
			);
		},

		getDraft(draftId) {
			return api.prod.get(`/slateDrafts/${draftId}`).then(
					res => {
						return res.data;
					}
			);
		},
		
		getAllDrafts() {
			return api.prod.get(`/slateDrafts`).then(
					res => res.data
			);
		},

		deleteSlate(slateId) {
			return $q((resolve, reject) => {
				resolve({status: 'success'})
			});
		},

		getGameSlates(gameId, sortBy) {
			return api.prod.get(`/slates`, {
				params: {
					game: gameId,
					sortBy: sortBy,
					limit: 100
				}
			});
		},

		vote(slateId, direction) {
			return api.prod.post(`/slates/${slateId}/vote`, {
				direction: direction
			});
		}
	}
};