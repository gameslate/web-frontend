module.exports = function($rootScope) {
	"ngInject";

	let loadingCount = 0;

	function broadcastState() {
		$rootScope.$broadcast('$loadingStateChange', loadingCount!==0);
	}

	return {
		start() {
			loadingCount++;
			broadcastState();
		},
		end() {
			loadingCount--;
			broadcastState();
		},
		endAll() {
			loadingCount = 0;
			broadcastState();
		},
		getLoadingState() {
			return loadingCount!==0;
		}
	}
};