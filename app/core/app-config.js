module.exports = function(formlyConfigProvider, $urlRouterProvider, $locationProvider, $httpProvider, cloudinaryProvider) {
	"ngInject";

	$urlRouterProvider.otherwise('/');
	$locationProvider.html5Mode(true);
	$httpProvider.useApplyAsync(true);
	$httpProvider.interceptors.push('httpInterceptor');

	// formly custom templates
	formlyConfigProvider.setWrapper({
		name: 'prefixInput',
		template: `
			<div class="form__input-prefix-container">
				<label for="{{::id}}" class="control-label">
          {{to.label}} {{to.required ? '*' : ''}}
        </label>
				<div class="form__input-prefix">{{to.prefix}}</div>
				<formly-transclude></formly-transclude>
			</div>
		`
	});

	formlyConfigProvider.setType({
		name: 'prefixInput',
		extends: 'input',
		wrapper: ['prefixInput', 'bootstrapHasError']
	});

	cloudinaryProvider.config({
		upload_endpoint: 'https://api.cloudinary.com/v1_1/',
		cloud_name: 'game-slate'
	});
};