import * as api from '../core/api';

/**
 * This handles intercepting both $http and axios calls.
 */
module.exports = function($injector) {
	"ngInject";

	let authSvc = null;

	function interceptReq(config) {
		if (!authSvc) {
			authSvc = $injector.get('authSvc');
		}
		if(api.isCall(config.url)) {
			if(authSvc.isAuthenticated()) {
				config.headers.Authorization = authSvc.getAuthToken();
			}
		}
		//return modified config
		return config;
	}

	function interceptResSuccess(res) {
		if (authSvc.isAuthenticated() && authSvc.isTokenExpired()) {
			authSvc.signOut('token')
		}
		return Promise.resolve(res);
	}

	function interceptResError(res) {
		if (authSvc.isAuthenticated() && authSvc.isTokenExpired()) {
			authSvc.signOut('token')
		}
		return Promise.reject(res);
	}


	// attach to axios instance
	api.prod.interceptors.request.use(interceptReq);
	api.prod.interceptors.response.use(interceptResSuccess, interceptResError);
	
	// return service for angular
	return {
		request: interceptReq
	};
};