let axios = require('axios');

// test server api locally
//__DEV__ = false;

let prodApi = '/api/v1';
let devApi = 'http://localhost:8888/v1';

let API_BASE_URL = __DEV__ ? devApi : prodApi;
let MOCK_API_BASE_URL = 'http://localhost:7001/api/v1';

let responseType = 'json';

module.exports.prod = axios.create({
	baseURL: API_BASE_URL,
	responseType: responseType,
	// headers: {
	// 	'cache-control': 'no-cache'
	// }
});

module.exports.mock = axios.create({
	baseURL: MOCK_API_BASE_URL,
	responseType: responseType
});

module.exports.isCall = function(url) {
	return url.indexOf(API_BASE_URL) > -1 && url.indexOf('cloudinary') == -1;
};