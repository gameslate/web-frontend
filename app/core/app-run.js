module.exports = function($rootScope, $state, authSvc) {
	"ngInject";

	$rootScope.$on('$stateChangeSuccess', function(e, toState, toParams, fromState) {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	});

};