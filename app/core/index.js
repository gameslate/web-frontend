module.exports = function(app) {
	app.config(require('./app-config'));
	app.run(require('./app-run'));
	app.factory('httpInterceptor', require('./http-interceptor-svc'));
};