let _isNull = require('lodash/isNull');

module.exports = function() {
	var currentScope = null,
			body = angular.element(document.getElementsByTagName('body')),
			currentElement = null;
	return {
		openDropdown: function(scope) {
			if (_isNull(currentScope)) {
				currentScope = scope;
				currentElement = currentScope.content;
				body.append(currentScope.content);

			} else if (currentScope != scope) {
				currentScope.closeDropdown();
				currentElement.remove();
				currentScope = scope;
				currentElement = currentScope.content;
				body.append(currentScope.content);
			}
		}
	};
};