module.exports = function(app) {
	let dropdownSvc = require('./dropdown-svc');
	let dropdown = require('./dropdown');
	let dropdownBtn = require('./dropdown-btn');
	let dropdownContent = require('./dropdown-content');

	app.service('dropdownSvc', dropdownSvc);
	app.directive('dropdown', dropdown);
	app.directive('dropdownBtn', dropdownBtn);
	app.directive('dropdownContent', dropdownContent);
};