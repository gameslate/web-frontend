let _isUndefined = require('lodash/isUndefined');
let _isString = require('lodash/isString');

module.exports = function($compile, $window, $document, dropdownSvc) {
"ngInject";
	var dropdownCtrl = function($scope) {
		"ngInject";
		// set component options
		var opts = {
			stopBubbling: !_isUndefined($scope.stopBubbling),
			cover: !_isUndefined($scope.cover),
			padding: !_isUndefined($scope.cover)?0:3, // padding from from trigger (px)
			lean: _isString($scope.lean)&&$scope.lean=='left'||$scope.lean=='right'?$scope.lean:null
		};

		// set private variables
		var buttonRect = null,
				self = this;
		$scope.content = null;
		$scope.button = null;



		// get content from child
		this.setContent = function(content) {
			$scope.content = content;

			// stop bubbling on element click
			if (opts.stopBubbling) {
				$scope.content.on('click', function(e) {
					e.stopPropagation();
				});
			}

		};

		// get button from child
		this.setButton = function(button) {
			$scope.button = button;
		};


		this.openDropdown = function() {
			// get viewport dimensions
			var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
			var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

			// set current scope of dropdown
			dropdownSvc.openDropdown($scope);

			// activate content and button
			$scope.content.addClass('active');
			$scope.button.addClass('active');

			// bind document clicks
			bindClick();

			// get button bounding rect
			buttonRect = $scope.button[0].getBoundingClientRect();
			var coverOffset = opts.cover?buttonRect.height:0;
			if (opts.cover) {
				$scope.content[0].style.width = buttonRect.width + 'px';
			}

			if (opts.lean=='right') {
				leanRight();
			} else if (opts.lean=='left') {
				leanLeft();
			} else {
				// check if there's room on the right
				if (w<=(buttonRect.left + $scope.content[0].offsetWidth)) {
					leanLeft();
				} else {
					leanRight();
				}
			}


			// check if there's room on the bottom
			if ($scope.content[0].offsetHeight+opts.padding-coverOffset>h-buttonRect.top-buttonRect.height) {
				$scope.content[0].style.top = buttonRect.top-$scope.content[0].offsetHeight-opts.padding+coverOffset + 'px';
			} else {
				$scope.content[0].style.top = buttonRect.top+buttonRect.height+opts.padding-coverOffset + 'px';
			}
		};

		function leanLeft() {
			$scope.content[0].style.left = buttonRect.left-$scope.content[0].offsetWidth+buttonRect.width + 'px';
		}

		function leanRight() {
			$scope.content[0].style.left = buttonRect.left + 'px';
		}

		// allow service to close dropdown
		$scope.closeDropdown = function() {
			self.closeDropdown();
		};

		// allow child directives to close dropdown
		this.closeDropdown = function() {
			documentClose()
		};

		// bind document click and scroll event
		function bindClick() {
			$document.on('click', documentClose);
			angular.element($window).on('resize', scrollEvent);
		}

		// unbind document click and scroll event
		function unbindClick() {
			$document.off('click', documentClose);
			angular.element($window).off('resize', scrollEvent);
		}

		// on document click, close dropdown
		function documentClose() {
			$scope.content.removeClass('active');
			$scope.button.removeClass('active');
			unbindClick();
		}

		function scrollEvent() {
			self.closeDropdown();
		}

		// destroy document bind if it's still on
		$scope.$on('$destroy', function() {
			documentClose();
			$document.off('click', documentClose);
			angular.element($window).off('resize', scrollEvent);
		})
	};

	return {
		restrict: 'A',
		scope: {
			stopBubbling: '@',
			lean: '@',
			cover: '@'
		},
		controller: dropdownCtrl
	}
};