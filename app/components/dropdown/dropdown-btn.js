module.exports = function() {
	var ddBtnLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.setButton(element);

		element.on('click', function(e) {
			e.stopPropagation();
			if (element.hasClass('active')) {
				parentCtrl.closeDropdown();
			} else {
				parentCtrl.openDropdown();
			}
		})
	};
	return {
		restrict: 'A',
		require: '^dropdown',
		link: ddBtnLink
	}
};