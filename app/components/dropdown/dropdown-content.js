module.exports = function() {
	var ddContentLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.setContent(element);
	};
	return {
		restrict: 'C',
		require: '^dropdown',
		link: ddContentLink
	}
};