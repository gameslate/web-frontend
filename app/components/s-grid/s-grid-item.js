module.exports = function() {
	let link = function(scope, element, attrs, ctrl) {
		ctrl.addGridItem(element);
	};

	return {
		restrict: 'A',
		require: '^sGrid',
		link: link
	};
};