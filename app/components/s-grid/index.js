module.exports = function(app) {
	app.directive('sGrid', require('./s-grid'));
	app.directive('sGridItem', require('./s-grid-item'));
};