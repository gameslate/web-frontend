import _debounce from 'lodash/debounce';
import _forEach from 'lodash/forEach';

module.exports = function() {
	let ctrl = function($element, $window) {
		"ngInject";

		$element[0].style.position = 'relative';
		$element[0].style.display = 'block';

		let vm = this,
				gridElements = [],
				parentContainer = angular.element($element[0].parentElement),
				currentParentWidth = parentContainer[0].offsetWidth;

		let colGutter = 40,
				colWidth = vm.colWidth ? vm.colWidth: 240;

		// key: width
		// value: columns
		let rules = width => {
			if (width < (colWidth*2) + colGutter) {
				return 1;
			} else if (width < (colWidth*3) + (colGutter*2)) {
				return 2;
			} else if (width < (colWidth*4) + (colGutter*3)) {
				return 3;
			}	else if (width < (colWidth*5) + (colGutter*4)) {
				return 4;
			} else if (width < (colWidth*6) + (colGutter*5)) {
				return 5;
			} else if (width < (colWidth*7) + (colGutter*6)) {
				return 6;
			}	else if (width < (colWidth*8) + (colGutter*7)) {
				return 7;
			} else if (width < (colWidth*9) + (colGutter*8)) {
				return 8;
			} else if (width < (colWidth*10) + (colGutter*9)) {
				return 9;
			}	else if (width < (colWidth*11) + (colGutter*10)) {
				return 10;
			}
		};

		this.addGridItem = element => {
			gridElements.push(element);
			resizeCards();
		};

		$window.addEventListener('resize', _debounce(function() {
			if (currentParentWidth != parentContainer[0].offsetWidth) {
				currentParentWidth = parentContainer[0].offsetWidth;
				resizeCards();
			}
		}, 100));

		let resizeCards = () => {
			_forEach(gridElements, (card, i) => {
				card[0].style.width = `${100/rules(currentParentWidth)}%`;
			});
		};

		resizeCards();

	};

	let template = ``;

	return {
		restrict: 'E',
		scope: {
			colWidth: '='
		},
		controller: ctrl,
		controllerAs: 'sGrid',
		bindToController: true,
		template: template
	};
};