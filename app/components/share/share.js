export default function() {
	let ctrl = function(tsModalService) {
		"ngInject";

		this.close = () => {
			tsModalService.submit();
		};

		this.url = window.location.href;
	};

	let template = `
		<button class="btn btn-close" ng-click="share.close()">
		    <icon icon="close"></icon>
		</button>
		
		<div class="modal__header">
		    <h4>
		        Share {{share.type.slice(0, 1).toUpperCase() + share.type.slice(1)}}
		    </h4>
		</div>
		<div class="modal__body">
		    <div class="form-group">
		        <label>Copy and Paste URL</label>
		        <input type="text"
		               disabled
		               ng-model="share.url"
		               class="form-control">
		    </div>
		</div>
		<div class="modal__footer">
		    <button class="btn btn-primary" ng-click="share.close()">Done</button>
		</div>
	`;

	return {
		restrict: 'E',
		scope: {
			type: '='
		},
		controller: ctrl,
		controllerAs: 'share',
		bindToController: true,
		template: template
	};
};