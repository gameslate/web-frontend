export default function(tsModalService) {
	"ngInject";

	function share(type) {
		return tsModalService.open({
			directive: 'share',
			size: 'small',
			resolve: {
				type: type
			}
		});
	}

	return {
		shareGame() {
			share('game');
		},
		shareSlate() {
			share('slate');
		}
	};
};