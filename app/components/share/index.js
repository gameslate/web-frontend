import share from './share';
import shareSvc from './share-svc';

module.exports = function(app) {
	app.service('shareSvc', shareSvc);
	app.directive('share', share);
};