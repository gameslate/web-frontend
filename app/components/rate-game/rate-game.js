import _forEach from 'lodash/forEach';
import _filter from 'lodash/filter';
import state from '../../core/state';
import _size from 'lodash/size';
import _isNumber from 'lodash/isNumber';

export default function() {
	let ctrl = function($scope, tsModalService, gameSvc, rateGameSvc) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, vm);
		vm.state.set({
			component: 'score' // score | error
		});
		vm.data = vm.data || {};
		vm.game = gameSvc.getCurrentGame();

		vm.submit = () => {
			if (everythingGotTouched()) {
				rateGameSvc.rate(vm.gameUri, createSimpleObject()).then(
						res => tsModalService.submit()
				);
			} else {
				vm.state.set({
					component: 'error'
				})
			}
		};

		vm.close = () => tsModalService.cancel();

		vm.rating = {
			gameplay: {
				score: vm.data.gameplay || 0,
				touched: _isNumber(vm.data.gameplay)
			},
			sound: {
				score: vm.data.sound || 0,
				touched: _isNumber(vm.data.sound)
			},
			controls: {
				score: vm.data.controls || 0,
				touched: _isNumber(vm.data.controls)
			},
			story: {
				score: vm.data.story || 0,
				touched: _isNumber(vm.data.story)
			},
			graphics: {
				score: vm.data.graphics || 0,
				touched: _isNumber(vm.data.graphics)
			},
			replayValue: {
				score: vm.data.replayValue || 0,
				touched: _isNumber(vm.data.replayValue)
			},
			playTime: {
				score: vm.data.playTime || 0,
				touched: true
			}
		};

		function everythingGotTouched() {
			return _filter(vm.rating, rating => {
						return rating.touched;
					}).length==_size(vm.rating);
		}

		function createSimpleObject() {
			let newObject = {};
			_forEach(vm.rating, (rating, key) => {
				newObject[key] = rating.score;
			});
			return newObject;
		}

		$scope.$watch(() => {
			return vm.tsModalReady;
		}, newVal => {
			if (newVal) $scope.$broadcast('refreshSlider');
		})
	};
	
	return {
		restrict: 'E',
		scope: {
			gameUri: '=',
			data: '=',
			tsModalReady: '='
		},
		controller: ctrl,
		controllerAs: 'rateGame',
		bindToController: true,
		template: require('./rate-game.html')
	};
};