import rateGame from './rate-game';
import rateGameSvc from './rate-game-svc';

module.exports = function(app) {
	app.directive('rateGame', rateGame);
	app.service('rateGameSvc', rateGameSvc);
};