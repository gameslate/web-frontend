import api from '../../core/api';

export default function(tsModalService) {
	"ngInject";

	return {
		open(gameUri, data) {
			return tsModalService.open({
				directive: 'rateGame',
				closeBackdrop: false,
				resolve: { gameUri, data }
			});
		},
		rate(gameUri, scores) {
			return api.prod.post(`/games/${gameUri}/stats`, scores).then(
					res => res.data
			);
		}
	};
};