import state from '../../core/state';

module.exports = function() {

	let wishlistBtnCtrl = function($scope, authSvc, toasterSvc) {
		"ngInject";

		let vm = this;
		vm.game = vm.gameObject;
		vm.state = new state($scope, vm);
		vm.state.set({
			isAuthenticated: authSvc.isAuthenticated()
		});

		if (authSvc.isAuthenticated()) {
			vm.state.set({
				status: authSvc.isGameInWishlist(vm.game._id)
			});
		}

		let toggle = () => {
			if (vm.state.status) {
				vm.state.set({status: false});
				authSvc.removeWishlist(vm.game._id).then(
						null,
						error => {
							vm.state.set({status: true});
							toasterSvc.error(`Couldn't update your wishlist`, `Error Occurred`);
							console.error(error);
						}
				);
			} else {
				vm.state.set({status: true});
				authSvc.addWishlist(vm.game).then(
						null,
						error => {
							vm.state.set({status: false});
							toasterSvc.error(`Couldn't update your wishlist`, `Error Occurred`);
							console.error(error);
						}

				);
			}
		};

		vm.clickHandler = () => {
			if (vm.state.isAuthenticated) {
				toggle();
			} else {
				authSvc.signInModal({
					title: 'Whoops',
					message: 'You must be signed in to add this game to your wishlist.'
				});
			}
		};
	};

	let template = `
			<button class="btn btn-white-transparent" ng-click="wishlistBtn.clickHandler()">
				<span ng-hide="wishlistBtn.state.status">
					<icon icon="add"></icon>
					Add to Wishlist
				</span>
				<span ng-show="wishlistBtn.state.status">
					<icon icon="remove"></icon>
					Remove from Wishlist
				</span>
			</button>
		`;

	return {
		restrict: 'E',
		replace: true,
		scope: {gameObject: '='},
		controller: wishlistBtnCtrl,
		controllerAs: 'wishlistBtn',
		bindToController: true,
		template: template
	};
};