import state from '../../core/state';
let _isNull = require('lodash/isNull');

module.exports = function() {

	var thumbRatingCtrl = function($scope, slateSvc, authSvc) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, vm);
		vm.state.set({
			userRating: vm.slate.userRating,
			upVotes: vm.slate.upVotes,
			downVotes: vm.slate.downVotes
		});

		if (authSvc.getUser('me') && vm.slate.author._id == authSvc.getUser('me')._id) {
			vm.hideModule = true;
		}

		let vote = () => {
			slateSvc.vote(vm.slate._id, vm.state.userRating).then(

			);
		};

		vm.rate = function(direction) {
			if (authSvc.getUser('me')) {
				if (vm.state.userRating == 'null' || _isNull(vm.state.userRating)) {
					vm.state.set({
						[direction+'Votes']: vm.state[direction+'Votes']+1,
						userRating: direction
					}, vote);
				} else if (vm.state.userRating == direction) {
					vm.state.set({
						[direction+'Votes']: vm.state[direction+'Votes']-1,
						userRating: 'null'
					}, vote);
				} else {
					vm.state.set({
						[direction+'Votes']: vm.state[direction+'Votes']+1,
						[`${direction=='up'?'down':'up'}Votes`]: vm.state[`${direction=='up'?'down':'up'}Votes`]-1,
						userRating: direction
					}, vote);

				}
			} else {
				authSvc.signInModal({message: 'you must be signed in to rate this slate'});
			}
		};

		$scope.$watch(() => {
			return vm.slate;
		}, newVal => {
			if (newVal) {
				vm.state.set({rating: newVal.userRating});
			}
		});
	};

	return {
		restrict: 'E',
		scope: {
			slate: '='
		},
		controller: thumbRatingCtrl,
		controllerAs: 'thumb',
		bindToController: true,
		template: require('./thumb-rating.html')
	};
};