module.exports = function() {

	let slateBoxCtrl = function($filter, userSvc, $state) {
		"ngInject";

		let vm = this;
		vm.user = userSvc.get('me');
		if (vm.slate.coverImgUrl) {
			vm.slate.coverImgUrl = $filter('scaleImage')(vm.slate.coverImgUrl, 650);
		}
		
		vm.href = !!vm.draft
				? $state.href('edit', {id: vm.slate._id, type: 'draft'})
				: $state.href('slate', {slateUri: vm.slate.uri, username: vm.slate.author.username});
	};

	let template = `
			<a class="slate-box"
				 ng-href="{{slateBox.href}}">
				<div class="slate-box__contain">
					<div class="slate-box__details"
							 ng-class="{'slate-box__image': slateBox.slate.coverImgUrl, 'slate-box__draft': slateBox.draft}">
							 <div class="slate-box__blur"
							      ng-style="{'backgroundImage': 'url('+slateBox.slate.coverImgUrl+')'}"></div>
							 
						<!-- draft label -->
						<span class="label label-primary slate-box__draft-label"
									ng-show="slateBox.draft">
							Draft
						</span>
						
						<!-- thumb rating -->
						<div class="slate-box__thumb" ng-hide="slateBox.draft">
							<div class="slate-box__thumb-icon">
								<icon icon="thumb-up"></icon>
							</div>
							<div class="slate-box__thumb-rating">
								{{slateBox.slate.upVotes}}
							</div>
						</div>

						<!-- author & date-->
						<div class="slate-box__author">
							<div class="slate-box__author-img"
									 profile-image="" color="!slateBox.slate.coverImgUrl?'grey':'white'">
							</div>
							<div class="slate-box__author-handle ellipsis">
								@{{slateBox.draft?slateBox.user.username:slateBox.slate.author.username}}
							</div>
							<div class="slate-box__author-published">
								<span am-time-ago="slateBox.slate.publishedDate" ng-hide="slateBox.draft"></span>
								<span ng-show="slateBox.draft">Not Published Yet</span>
							</div>
						</div>
					</div>
					<div class="slate-box__content">
						<div class="slate-box__title ellipsis">
							{{slateBox.slate.title ? slateBox.slate.title : 'Untitled Slate'}}
						</div>
						<div class="slate-box__lead ellipsis" ng-show="slateBox.slate.subtitle">
							{{slateBox.slate.subtitle}}
						</div>
						<div class="slate-box__excerpt ellipsis"
								 ng-show="!slateBox.slate.subtitle"
								 ng-bind-html="slateBox.slate.excerpt"></div>
						<div class="slate-box__game ellipsis">
							{{slateBox.slate.game.title}}
						</div>
					</div>
				</div>
			</a>
		`;

	return {
		restrict: 'E',
		scope: {
			slate: '=',
			draft: '@'
		},
		controller: slateBoxCtrl,
		controllerAs: 'slateBox',
		bindToController: true,
		template: template
	};

};
