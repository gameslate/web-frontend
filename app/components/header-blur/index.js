module.exports = function($filter) {
	"ngInject";
	var headerBlurLink = function(scope, element) {

		let imageEl = angular.element(`<header-blur-child></header-blur-child>`);

		if (scope.imageUrl) {
			showBackground(scope.imageUrl);
		}

		function showBackground(backgroundUrl) {
			element.prepend(imageEl);
			imageEl[0].style.backgroundImage = `url(${$filter('scaleImage')(backgroundUrl, 650)})`;
			imageEl[0].style.opacity = .4;
		}
	};

	return {
		restrict: 'E',
		scope: {
			imageUrl: '='
		},
		link: headerBlurLink
	}
};