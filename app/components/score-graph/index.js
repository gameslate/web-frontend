let _isUndefined = require('lodash/isUndefined');
import _isEmpty from 'lodash/isEmpty';

module.exports = function($filter) {
	"ngInject";
	var scoreGraphCtrl = function($scope, $element) {
		"ngInject";
		let d3 = require('d3');

		function getTotal(score) {
			return ((score.gameplay + score.sound +
			score.controls + score.story +
			score.graphics + score.replayValue) / 6).toFixed(1);
		}

		function buildGraph() {
			// empty element in case it's a rebuild
			$element.empty();

			var score = [getTotal($scope.score), 10-getTotal($scope.score)];

			var width = !_isUndefined($scope.small)?85:110,
					height = !_isUndefined($scope.small)?85:110,
					radius = Math.min(width, height) / 2;

			var color = [$filter('ratingColor')(score[0]), '#e3e3e3'];

			var pie = d3.layout.pie();

			var arc = d3.svg.arc()
					.innerRadius(radius)
					.outerRadius(radius - (!_isUndefined($scope.small)?12:15));

			var svg = d3.select($element[0]).append("svg")
					.attr("width", width)
					.attr("height", height)
					.append("g")
					.attr("transform", "translate(" + width / 2 + "," + height / 2 +  ")");

			svg.append("text")
					.attr('text-anchor', 'middle')
					.attr('y', (!_isUndefined($scope.small)?'9px':'13px'))
					.attr('itemProp', 'aggregateRating')
					.style({
						'font-size': !_isUndefined($scope.small)?'26px':'36px',
						'font-weight': 700
					})
					.style('fill', color[0])
					.text(score[0]);


			var path = svg.selectAll("path")
					.data(pie(score))
					.enter().append("path")
					.attr("fill", function(d, i) { return color[i]; })
					.attr("d", arc);

		}

		// on change, rebuild graph
		$scope.$watch('score', (newVal) => {
			if (newVal && !_isEmpty(newVal)) {
				buildGraph();
			}
		});

		if ($scope.score && !_isEmpty($scope.score)) {
			buildGraph();
		}
	};

	return {
		scope: {
			score: '=',
			small: '@'
		},
		template: '<div></div>',
		controller: scoreGraphCtrl
	}

};