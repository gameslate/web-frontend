let tagHelpers = require('./tags-helpers');

module.exports = function() {
	let tagsCtrl = function($scope, $document, tagSvc) {
		"ngInject";

		const typePlaceholder = 'Add a Tag';

		let vm = this;
		vm.data = vm.bindData;
		vm.state = {
			component: 'type', // type | tag
			input: '',
			hideInput: vm.edit!='true',
			placeholder: typePlaceholder,
			dropdown: false,
			typeContent: 'loading', // loading | hasData | error
			tagContent: 'loading', // loading | hasData | error
			typeResults: null,
			tagResults: null,
			typeSelection: null,
			tagSelection: null
		};

		vm.searchTypes = search => {
			this.state.typeContent = 'loading';
			tagSvc.getTagTypes(search).then(
				res => {
					vm.state.typeResults = res;
					this.state.typeContent = 'hasData';
				}
			);
		};

		// load preliminary type results
		// ---------------------------------------
		vm.searchTypes();

		vm.searchTags = (search='') => {
			this.state.tagContent = 'loading';
			tagSvc.getTags(search).then(
				res => {
					vm.state.tagResults = res;
					this.state.tagContent = 'hasData';
				}
			);
		};

		vm.chooseType = (tag, type) => {
			$scope.$applyAsync(() => {
				vm.state.component = 'tag';
				vm.state.placeholder = tagHelpers.createPlaceholder(type, tag.name)
				vm.state.typeSelection = tag;
				vm.state.typeSelection.type = type;
				vm.searchTags();
			});
		};

		vm.openDropdown = (e) => {
			vm.state.dropdown = true;
		};

		vm.closeDropdown = (e) => {
			if (e.target.id !== 'tag-input') {
				$scope.$applyAsync(() => {
					vm.state.dropdown = false;
				});
			}
		};

		vm.reset = () => {
			$scope.$applyAsync(() => {
				vm.state = {
					component: 'type',
					input: '',
					placeholder: typePlaceholder,
					dropdown: false,
					typeContent: 'loading',
					tagContent: 'loading',
					typeResults: null,
					tagResults: null,
					typeSelection: null,
					tagSelection: null
				};
				vm.searchTypes();
			});
		};

		vm.addTag = (tag) => {
			vm.data.push(tag);
			vm.reset();
		};

		$document.on('click', vm.closeDropdown);


		function toggleDropdown(e) {

		}

		$scope.$on('$destroy', () => {
			$document.off('click', vm.closeDropdown);
		});
	};

	return {
		restrict: 'E',
		scope: {
			bindData: '=data',
			saveCb: '&save',
			edit: '@'
		},
		controller: tagsCtrl,
		controllerAs: 'tags',
		bindToController: true,
		template: `
<div class="tags">
    <div class="tag tag--{{tag.type}}"
         ng-class="{'tag__view-only': tags.state.hideInput}"
         ng-repeat="tag in tags.data">
        <div class="tag__key">
            {{::tag.name}}
        </div><div class="tag__val">
            {{::tag.val}}
        </div><div class="tag__close" ng-if="!tags.state.hideInput">
            <icon icon="close"></icon>
        </div>
    </div
    ><div class="tag tag--{{tags.state.typeSelection.type}} tag-input__{{tags.state.component}}">
        <div class="tag__key" ng-show="tags.state.component=='tag'">
            {{tags.state.typeSelection.abbr?tags.state.typeSelection.abbr:tags.state.typeSelection.name}}
            <div class="tag__close" ng-click="tags.reset()">
                <icon icon="close"></icon>
            </div>
        </div
        ><div class="tag__input" ng-if="!tags.state.hideInput">
            <input id="tag-input"
                   ng-focus="tags.openDropdown()"
                   type="text"
                   placeholder="{{tags.state.placeholder}}">
            <div class="tag__dropdown" ng-show="tags.state.dropdown">
                <div class="dropdown-content__contain">
                    <div ng-show="tags.state.component == 'type'">
                        <ul class="list-select" ng-repeat="type in tags.state.typeResults">
                            <li class="title text-uppercase">
                                {{type.type}}
                            </li>
                            <li ng-repeat="row in type.data">
                                <a ng-click="tags.chooseType(row, type.type)">
                                    {{row.abbr?row.abbr:row.name}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div ng-show="tags.state.component == 'tag'">
                        <ul class="list-select">
                            <li class="title text-uppercase">
                                Tag Suggestions
                            </li>
                            <li ng-repeat="tag in tags.state.tagResults">
                                <a ng-click="tags.addTag(tag)">
                                    {{tag.val}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`
	};
};