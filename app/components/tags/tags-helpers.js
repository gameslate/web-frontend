module.exports = {

	createPlaceholder: (type, selection) => {
		switch(type) {
			case 'media':
				return `Which ${(selection.slice(0, 1)).toUpperCase()+selection.slice(1)}?`;

			case 'genre':
				return `Tag '${selection}'`;

			case 'category':
				return `Tag '${selection}'`;
		}
	}

};