
angular.module('angular-medium-editor', [])

	.directive('mediumEditor', function() {

		function toInnerText(value) {
			var tempEl = document.createElement('div'),
				text;
			tempEl.innerHTML = value;
			text = tempEl.textContent || '';
			return text.trim();
		}

		function isNewLine(editable) {
			return editable.innerHTML.trim() == '<p><br></p>';
		}

		return {
			require: 'ngModel',
			restrict: 'AE',
			scope: {
				bindOptions: '=',
				bindKeyPress: '&'
			},
			link: function(scope, iElement, iAttrs, ngModel) {

				angular.element(iElement).addClass('angular-medium-editor');

				scope.bindOptions.extensions = {
					'upload-img': new UploadImgExtension()
				};

				// Global MediumEditor
				ngModel.editor = new MediumEditor(iElement, scope.bindOptions);

				ngModel.$render = function() {
					iElement.html(ngModel.$viewValue || "");
					ngModel.editor.getExtensionByName('placeholder').updatePlaceholder(iElement[0]);
				};

				ngModel.$isEmpty = function(value) {
					if (/[<>]/.test(value)) {
						return toInnerText(value).length === 0;
					} else if (value) {
						return value.length === 0;
					} else {
						return true;
					}
				};

				ngModel.editor.subscribe('editableInput', function (event, editable) {
					scope.bindKeyPress();
					ngModel.$setViewValue(editable.innerHTML.trim());
				});

				scope.$watch('bindOptions', function(bindOptions) {
					ngModel.editor.init(iElement, bindOptions);
				});
			}
		};

	});