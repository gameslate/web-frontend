let _isNull = require('lodash/isNull');

module.exports = function() {
	var mainNavCtrl = function($scope, $element, gameSvc, $state, slateSvc, searchSvc, loadSvc, $document, $window, authSvc) {
		"ngInject";

		let vm = this,
				scrolled = null,
				drawer = null,
				drawerWrapper = null;

		// set current loading state
		vm.isLoading = loadSvc.getLoadingState();
		vm.showSearch = $state.current.name !== 'explore';
		vm.isAuthenticated = authSvc.isAuthenticated();
		vm.backgroundColor = 'dark';
		if (vm.isAuthenticated) {
			vm.user = authSvc.getUser();
		}

		vm.createSlate = function() {
			slateSvc.createSlate().then(
				res => {
					$state.go('editSlate', {
						slateId: res.id,
						tags: $state.current.name==='game'?
							[{key: 'game', val: gameSvc.getCurrentGame().title}]:null
					})
				},
				error => {
					console.error(error);
				}
			);
		};

		vm.addDrawer = (element) => {
			drawer = element;
		};

		vm.addDrawerWrapper = (element) => {
			drawerWrapper = element;
		};

		vm.closeDrawer = () => {
			Velocity(drawer, {
				translateX: 300
			}, {
				display: 'none'
			});
			Velocity(drawerWrapper, {
				opacity: 0
			}, {
				display: 'none'
			})
		};

		vm.openDrawer = () => {
			Velocity(drawer, {
				translateX: [0, 300]
			}, {
				display: 'block'
			});
			Velocity(drawerWrapper, {
				opacity: [1, 0]
			}, {
				display: 'block'
			})
		};

		vm.register = () => {
			authSvc.registerModal();
		};

		vm.signIn = () => {
			authSvc.signInModal();
		};

		vm.signOut = () => {
			authSvc.signOut();
		};

		// watch for loading state change
		$scope.$on('$loadingStateChange', function (e, loadingState) {
			$scope.$evalAsync(function () {
				vm.isLoading = loadingState;
			});
		});

		$scope.$watch(() => {return vm.background}, newVal => {
			chooseBackground(newVal);
		});

		function chooseBackground(bkg) {
			if (bkg === 'light') {
				$element.addClass('isLight');
				vm.backgroundColor = 'light';
			} else {
				$element.removeClass('isLight');
				vm.backgroundColor = 'dark';
			}
		}

		chooseBackground(vm.background);

		function fireScroll() {
			if ($window.pageYOffset>0 && (!scrolled || _isNull(scrolled))) {
				$element.addClass('isScrolled');
			} else if ($window.pageYOffset===0 && (scrolled || _isNull(scrolled))) {
				$element.removeClass('isScrolled');
			}
		}
		fireScroll();
		$document.on('scroll', fireScroll);

		$scope.$on('$destroy', () => {
			$document.off('scroll', fireScroll);
		});

	};
	return {
		restrict: 'E',
		replace: true,
		scope: {
			background: '='
		},
		template: require('./main-nav.html'),
		controller: mainNavCtrl,
		controllerAs: 'mainNav',
		bindToController: true
	}
};