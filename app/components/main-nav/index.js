module.exports = function(app) {
	let mainNav = require('./main-nav');
	let drawerWrapper = require('./drawer-wrapper');
	let drawer = require('./drawer');

	app.directive('mainNav', mainNav);
	app.directive('drawerWrapper', drawerWrapper);
	app.directive('drawer', drawer);
};