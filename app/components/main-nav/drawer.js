module.exports = function() {

	let drawerLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.addDrawer(element);
	};

	return {
		require: '^mainNav',
		link: drawerLink
	}
};