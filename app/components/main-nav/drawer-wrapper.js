module.exports = function() {

	let drawerWrapperLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.addDrawerWrapper(element);
	};

	return {
		require: '^mainNav',
		link: drawerWrapperLink
	}
};