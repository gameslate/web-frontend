module.exports = function() {

	let platformCtrl = function($scope, $element) {
		"ngInject";

		let vm = this;

		switch (vm.data.abbreviation) {
			case 'MAC':
				vm.icon = 'mac';
				break;
			case 'PC':
				vm.icon = 'pc';
				break;
			case 'PS4':
				vm.icon = 'ps';
				break;
			case 'PS3':
				vm.icon = 'ps';
				break;
			case 'PS2':
				vm.icon = 'ps';
				break;
			case 'VITA':
				vm.icon = 'ps';
				break;
			case 'XONE':
				vm.icon = 'xbox';
				break;
			case 'X360':
				vm.icon = 'xbox';
				break;
			case 'XBOX':
				vm.icon = 'xbox';
				break;
			case 'LIN':
				vm.icon = 'linux';
				break;
			case 'WIIU':
				vm.icon = 'nintendo';
				break;
			case 'Wii':
				vm.icon = 'nintendo';
				break;
			case '3DS':
				vm.icon = 'nintendo';
				break;
			case 'GB':
				vm.icon = 'nintendo';
				break;
			case 'GBA':
				vm.icon = 'nintendo';
				break;
			case 'NES':
				vm.icon = 'nintendo';
				break;
			case 'SNES':
				vm.icon = 'nintendo';
				break;
			case 'N64':
				vm.icon = 'nintendo';
				break;
			case 'DS':
				vm.icon = 'nintendo';
				break;
			case '64DD':
				vm.icon = 'nintendo';
				break;
			case 'GCN':
				vm.icon = 'nintendo';
				break;
			case 'GEN':
				vm.icon = 'sega';
				break;
			case 'DC':
				vm.icon = 'sega';
				break;
			default:
				$element.remove();
				$scope.$destroy();
				break;
		}
	};

	return {
		restrict: 'E',
		scope: {
			data: '='
		},
		controller: platformCtrl,
		controllerAs: 'platform',
		template: require('./platform.html'),
		bindToController: true
	};
};