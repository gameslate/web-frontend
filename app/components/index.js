import tsModal from 'ts-modal';

module.exports = function(app) {
	// components builds itself
	require('./main-nav')(app);
	require('./dropdown')(app);
	require('./image-slider')(app);
	require('./tabs')(app);
	require('./game-score')(app);
	require('./dialog')(app);
	require('./rate-game')(app);
	require('./share')(app);
	require('./s-grid')(app);
	tsModal(app);

	// init components
	app.directive('loader', require('./loader'));
	app.directive('headerBlur', require('./header-blur'));
	app.directive('profileImage', require('./profile-image'));
	app.directive('scoreGraph', require('./score-graph'));
	app.directive('platform', require('./platform'));
	app.directive('thumbRating', require('./thumb-rating'));
	app.directive('slateBox', require('./slate-box'));
	app.directive('alert', require('./alert'));
	app.directive('wishlistBtn', require('./wishlist-btn'));
	app.directive('footer', require('./footer'));
	app.directive('gameBox', require('./game-box'));
	app.controller('sharingHead', require('./sharing-head'));
};