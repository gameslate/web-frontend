export default function(tsModalService) {
	"ngInject";
	return {
		open: function(title, description, confirmText='Confirm', cancelText='Cancel') {
			var modalData = {
				title: title,
				description: description,
				confirmText: confirmText,
				cancelText: cancelText
			};

			return tsModalService.open({
				directive: 'dialogComponent',
				size: 'small',
				resolve: {
					data: modalData
				}
			});
		}};
};