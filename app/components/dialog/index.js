import dialogSvc from './dialog-svc';
import dialog from './dialog';

module.exports = function(app) {
	app.service('dialogSvc', dialogSvc);
	app.directive('dialogComponent', dialog);
};