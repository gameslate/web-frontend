export default function() {
	let ctrl = function(tsModalService) {
		"ngInject";

		this.ok = function () {
			tsModalService.submit();
		};

		this.cancel = function () {
			tsModalService.cancel();
		};
	};
	
	let template = `
		<button class="btn btn-close" ng-click="dialog.cancel()">
		    <icon icon="close"></icon>
		</button>
		
		<div class="modal__header">
		    <h4>{{dialog.data.title}}</h4>
		</div>
		
		<div class="modal__body">
		    <p>
		        {{dialog.data.description}}
		    </p>
		</div>
		<div class="modal__footer">
		    <button class="btn btn-primary" ng-click="dialog.ok()">
		        {{dialog.data.confirmText}}
		    </button>
		    <button class="btn btn-grey-transparent" ng-click="dialog.cancel()">
		        {{dialog.data.cancelText}}
		    </button>
		</div>
	`;
	
	return {
		controller: ctrl,
		scope: {
			data: '='
		},
		controllerAs: 'dialog',
		bindToController: true,
		template: template
	};
};