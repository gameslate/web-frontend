module.exports = function($rootScope) {
	"ngInject";
	var loaderLink = function(scope, element, attrs) {
		$rootScope.$on('$stateChangeStart', function() {
			animateIn();
		});

		$rootScope.$on('$stateChangeError', function() {
			animateOut();
		});
		$rootScope.$on('$stateChangeSuccess', function() {
			animateOut();
		});

		function animateIn() {
			Velocity(element[0], 'stop');
			Velocity(element[0], {
				opacity: 1
			}, {
				display: 'block'
			})
		}

		function animateOut() {
			Velocity(element[0], 'stop');
			Velocity(element[0], {
				opacity: 0
			}, {
				display: 'none'
			});
		}
	};

	return {
		restrict: 'E',
		replace: true,
		template: require('./loader.html'),
		link: loaderLink
	}
};