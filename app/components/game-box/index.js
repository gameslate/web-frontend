module.exports = function() {

	let gameBoxCtrl = function($filter) {
		"ngInject";

		let vm = this;
		vm.coverImg = vm.game.coverImgUrl ? $filter('scaleImage')(vm.game.coverImgUrl, 650) : '';
	};

	let template = `
		<div class="game-box">
			<!--<div dropdown class="game-box__actions" lean="left">-->
				<!--<button class="btn btn-square btn-white-transparent" dropdown-btn>-->
					<!--<icon icon="more-vert"></icon>-->
				<!--</button>-->
				<!--<div class="dropdown-content dropdown-content__sm">-->
					<!--<ul class="list-select">-->
						<!--<div class="list__header">-->
							<!--Actions-->
						<!--</div>-->
						<!--<li>-->
							<!--<a>Add to Wishlist</a>-->
						<!--</li>-->
						<!--<li>-->
							<!--<a>Write a Slate</a>-->
						<!--</li>-->
					<!--</ul>-->
				<!--</div>-->
			<!--</div>-->
			<a ui-sref="game({gameUri: gameBox.game.uri})" title="{{gameBox.game.title}}">
				<div class="game-box__cover-img" ng-style="{'backgroundImage': 'url('+gameBox.coverImg+')'}">
					<div class="game-box__no-img" ng-show="! gameBox.game.coverImgUrl">
						<icon icon="image"></icon>
					</div>
				</div>
				<div class="game-box__info">
					<div class="game-box__title ellipsis">
						{{gameBox.game.title}}
					</div>
					<div class="game-box__details ellipsis" ng-show="gameBox.game.originalReleaseDate">
						{{gameBox.game}} 
					</div>				
				</div>
			</a>
		</div>
		`;

	return {
		restrict: 'E',
		scope: {
			game: '=data'
		},
		controller: gameBoxCtrl,
		controllerAs: 'gameBox',
		bindToController: true,
		template: template
	};
};