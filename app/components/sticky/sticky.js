gameApp.service('stickySvc', function() {

	let stickyGroup = [];

	return {
		add(element, id) {
			stickyGroup.push({
				element: element,
				id: id
			});
		}
	};
});

gameApp.directive('stickyEL', function(stickySvc, appUtils) {

	let stickyElLink = function(scope, element, attrs) {
		let id = appUtils.guid();
		stickySvc.add(element, id)
	};

	return {
		restrict: 'A',
		scope: {
			sticky: '@'
		},
		link: stickyElLink
	};
});

gameApp.directive('sticky', function(stickySvc, $document) {
	let stickyLink = function(scope, element) {
		let placeholder = angular.element('<div></div>'),
			parent = element[0].parentNode,
			isSticky = false;
			parent.insertBefore(placeholder[0], element[0]);


		function stickCheck() {
			let scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
			if (scrollTop >= element[0].offsetTop && isSticky==false) {
				isSticky = true;

			} else if (scrollTop < element[0].offsetTop && isSticky==true) {
				isSticky = false;
			}
		}

		$document.on('scroll', stickCheck);

	};

	return {
		restrict: 'A',
		link: stickyLink
	};

});