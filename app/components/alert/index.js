module.exports = function() {
		let alertCtrl = function($scope, $element) {
			"ngInject";

			let vm = this;

			if (!vm.title) {
				vm.title = (vm.type.slice(0 ,1).toUpperCase()).concat(vm.type.slice(1));
			}
			vm.close = () => {
				$scope.$destroy();
				$element.remove();
			};
		};

		let template = `
	    <div class="alert alert-{{alert.type}}" ng-class="alert.class">
        <button class="btn btn-close" ng-click="alert.close()">
          <icon icon="close"></icon>
        </button>
        <h4 ng-if="alert.title">{{alert.title}}</h4>
        {{alert.message}}
	    </div>
		`;

		return {
			restrict: 'E',
			scope: {
				message: '=',
				type: '@',
				class: '@',
				title: '='
			},
			controller: alertCtrl,
			controllerAs: 'alert',
			bindToController: true,
			template: template
		}
	};