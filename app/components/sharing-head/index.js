import state from '../../core/state';
import _isObject from 'lodash/isObject';
import _isBoolean from 'lodash/isBoolean';

module.exports = function($scope, $filter, $state) {
	"ngInject";
	let vm = this;
	vm.state = new state($scope, this);
	vm.state.title = 'GameSlate';

	let removeTheS = image => {
		if (image.indexOf('https') > -1) {
			let index = image.indexOf('s');
			return image.slice(0, index) + image.slice(index+1);
		} else {
			return image;
		}
	};

	let defaultDescription = 'GameSlate is a Gaming Community where anyone can write articles and reviews, leave fine-tuned ratings, create lists, and discover new games.';

	$scope.$on('setTitle', function(e, data) {
		data = _isObject(data) ? data : {};

		vm.state.set({
			url:          `https://www.gameslate.io${$state.href($state.current.name)}`,
			description:  data.description || defaultDescription,
			image:        data.image ? removeTheS($filter('scaleImage')(data.image, '650')) : undefined,
			type:         data.type || 'website',
			title:        data.title,
			headerTitle:  data.title ? `${data.title} - GameSlate` : 'GameSlate',
			siteName:     'GameSlate',
			appId:        '1705384936382406'
		}, () => {
			window.prerenderReady = true;
		});
	});
};


// 		<meta property="og:type"         content="{{share.state.type}}" />
// 		<meta property="og:title"        content="{{share.state.title}}" />
// 		<meta property="og:description"  content="{{share.state.description}}" />
// 		<meta property="og:image"        content="{{share.state.image}}" />
