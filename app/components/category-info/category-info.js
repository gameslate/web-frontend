gameApp.service('categoryService', function($q, $modal) {
  return {
    getCategory: function(category) {
      return $q(function(resolve, reject) {
        resolve({
          name: 'Slate',
          id: 'slate',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis at nunc a imperdiet. Duis vel dictum neque, vel dignissim est. Ut in felis id ipsum gravida tincidunt. Curabitur quis nisi porttitor, auctor nibh quis, vehicula felis. Aenean pretium mauris justo, tempor luctus metus posuere in.'
        })
      });
    },
    infoDialog: function(category) {
      $modal.open({
        controller: 'categoryInfoCtrl',
        templateUrl: 'category-info',
        resolve: {
          category: function () {
            return 'slate';
          }
        }
      });
    }};
});

gameApp.controller('categoryInfoCtrl', ['$scope', '$modalInstance', 'category', 'categoryService',
  function($scope, $modalInstance, category, categoryService) {
    categoryService.getCategory(category)
      .then(function ok(data) {
        $scope.category = data;
      })
    $scope.ok = function() {
      $modalInstance.close();
    }
  }]);