module.exports = function(app) {
	let tabs = require('./tabs');
	let tabsItem = require('./tabs-item');
	let tabsContent = require('./tabs-content');
	let tabsSref = require('./tabs-sref');
	let tabsSrefItem = require('./tabs-sref-item');

	app.directive('tabs', tabs);
	app.directive('tabsItem', tabsItem);
	app.directive('tabsContent', tabsContent);
	app.directive('tabsSref', tabsSref);
	app.directive('tabsSrefItem', tabsSrefItem);
};