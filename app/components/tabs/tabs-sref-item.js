module.exports = function() {
	return {
		require: '^tabsSref',
		link: function(scope, element, attrs, tabsCtrl) {
			tabsCtrl.addTab(element, attrs.uiSref);
		}
	};
};