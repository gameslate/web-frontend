module.exports = function() {

	var tabContentLink = function(scope, element, attrs, tabCtrl) {
		element[0].style.display = 'none';
		tabCtrl.addTabContent(element, attrs.tabsContent);
	};

	return {
		restrict: 'A',
		require: '^tabs',
		link: tabContentLink
	};
};