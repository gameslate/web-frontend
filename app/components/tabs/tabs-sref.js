let _forEach = require('lodash/forEach');

/**
 * TabsSref
 * tabs based off of the router state
 */

module.exports = function($rootScope, $state) {
	"ngInject";
	var tabsCtrl = function($scope) {
		"ngInject";
		var tabs = [];
		var activeClass = $scope.activeClass?$scope.activeClass:'isActive';

		var init = function() {
			updateTab();
		};

		$rootScope.$on('$stateChangeSuccess', function(event, current) {
			updateTab();
		});

		this.addTab = function(element, name) {
			tabs.push({
				name: name,
				element: element
			});

			updateTab();
		};

		var updateTab = function() {
			_forEach(tabs, function(tab, key) {
				tab.name = tab.name.replace(/\s*\(.*?\)\s*/g, '');
				if (tab.name == $state.current.name) {
					tab.element.addClass(activeClass);
				} else {
					tab.element.removeClass(activeClass);
				}
			});
		};

		init();
	};

	return {
		restrict: 'A',
		replace: true,
		scope: {
			activeClass: '@'
		},
		controller: tabsCtrl
	};
};