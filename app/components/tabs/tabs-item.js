module.exports = function() {
	var tabItemLink = function(scope, element, attrs, tabCtrl) {
		tabCtrl.addTabItem(element, attrs.tabsItem);
		element.on('click', function() {
			if (!element.attr('disabled')) {
				tabCtrl.changeTab(attrs.tabsItem);
			}
		});
	};

	return {
		restrict: 'A',
		require: '^tabs',
		compile: function() {
			return {
				pre: tabItemLink
			}
		}
	};
};