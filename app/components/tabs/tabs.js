let _forEach = require('lodash/forEach');

module.exports = function() {
	var tabsCtrl = function($scope) {
		"ngInject";
		var tabItemList = [],
				tabContentList = [],
				activeTabName = '',
				activeClass = $scope.activeClass?$scope.activeClass:'isActive';

		this.changeTab = function(newTab) {
			var currentTab = '';
			_forEach(tabItemList, function(item) {
				if (item.name == newTab) {
					item.element.addClass(activeClass);
					currentTab = item.name;
				} else {
					item.element.removeClass(activeClass);
				}
			});

			_forEach(tabContentList, function(item) {
				if (item.name == currentTab) {
					item.element[0].style.display = '';
				} else {
					item.element[0].style.display = 'none';
				}
			});
		};

		this.addTabItem = function(element, elementName) {
			if (!tabItemList.length) {
				element.addClass('isActive');
			}
			tabItemList.push({element: element, name: elementName});
			if (element.hasClass(activeClass)) {
				activeTabName = elementName;
			}
		};

		this.addTabContent = function(element, elementName) {
			tabContentList.push({element: element, name: elementName});
			if (elementName == activeTabName) {
				element[0].style.display = '';
			}
		};

		this.getActiveClass = function() {
			return activeClass;
		};
	};

	return {
		restrict: 'A',
		scope: {
			activeClass: '@'
		},
		controller: tabsCtrl
	};
};
