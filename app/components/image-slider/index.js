module.exports = function(app) {
	let imageSlider = require('./image-slider');
	let imageSliderItem = require('./image-slider-item');
	let imageSliderIndicators = require('./image-slider-numbers');
	let imageSliderNumbers = require('./image-slider-numbers');

	app.directive('imageSlider', imageSlider);
	app.directive('imageSliderItem', imageSliderItem);
	app.directive('imageSliderIndicators', imageSliderIndicators);
	app.directive('imageSliderNumbers', imageSliderNumbers);
};