module.exports = function() {
	var sliderNumbersLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.addNumbers(element);
	};

	return {
		restrict: 'E',
		require: '^imageSlider',
		link: sliderNumbersLink
	}
};