module.exports = function() {

	var sliderItemLink = function(scope, element, attrs, parentCtrl) {
		if (attrs.background || attrs.coverImg) parentCtrl.addItem(element, attrs.background, attrs.coverImg, attrs.backgroundColor);
	};

	return {
		restrict: 'E',
		require: '^imageSlider',
		link: sliderItemLink
	}
};