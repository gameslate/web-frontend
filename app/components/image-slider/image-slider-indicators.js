module.exports = function() {
	var sliderIndicatorsLink = function(scope, element, attrs, parentCtrl) {
		parentCtrl.addIndicators(element);
	};

	return {
		restrict: 'E',
		require: '^imageSlider',
		link: sliderIndicatorsLink
	}
};