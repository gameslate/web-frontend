let _isUndefined = require('lodash/isUndefined');
let _isNumber = require('lodash/isNumber');
let _throttle = require('lodash/throttle');

module.exports = function($compile, $interval) {
	"ngInject";
	var sliderCtrl = function($scope, $element, $filter) {
		"ngInject";
		var items = [],
				indicators = [],
				currentIndex = 0,
				animateEase = 'easeInOutQuart',
				interval,
				nextBtn,
				bar,
				previousBtn,
				opts = {
					backdropOpacity: _isNumber($scope.backdropOpacity)?$scope.backdropOpacity:.4,
					duration: _isNumber($scope.duration)?$scope.duration:10000,
					durationBar: !_isUndefined($scope.durationBar),
					auto: _isUndefined($scope.auto)
				},
				templates = {
					sliderItemParent: `<div class="slider-item-container"></div>`,
					imageItem: `<div class="slider-image"></div>`,
					sliderContent: `<div class="slider-content"></div>`,
					nextBtn: `<button class="slider-btn slider-next" ng-click="debounceNext();"><icon icon="chevron-right"></icon></button>`,
					previousBtn: `<button class="slider-btn slider-previous" ng-click="debouncePrevious();"><icon icon="chevron-left"></icon></button>`,
					indicator: `<div class="indicator"></div>`,
					bar: `<div class="slider-bar"></div>`,
					noImage: `<div class="slider__no-image"><icon icon="file-image"></icon><span>No Images</span></div>`,
					coverImg: `<div class="image-slider__cover-img"></div>`
				};

		let noImage = $compile(templates.noImage)($scope);
		$element.append(noImage);

		function addMulti() {
			$element[0].addEventListener('mouseover', stopInterval);
			$element[0].addEventListener('mouseleave', startInterval);

			nextBtn = angular.element(templates.nextBtn);
			previousBtn = angular.element(templates.previousBtn);
			$compile(nextBtn)($scope);
			$compile(previousBtn)($scope);
			$element.append(nextBtn).append(previousBtn);
			if (opts.durationBar) durationBar();

			startInterval();
		}

		function durationBar() {
			bar = angular.element(templates.bar);
			$compile(bar)($scope);
			$element.append(bar);
		}

		function start() {
			items[currentIndex].sliderParent[0].style.display = 'block';
		}

		function startInterval() {
			if (opts.auto) {
				if (opts.durationBar) animate.progress();
				interval = $interval(function() {
					if (opts.durationBar) animate.progress();
					$scope.debounceNext();
				}, opts.duration);
			}
		}

		function stopInterval() {
			if (opts.auto) {
				if (opts.durationBar) animate.progressStop();
				$interval.cancel(interval);
			}
		}

		function getNextItem() {
			if (currentIndex==items.length-1) {
				return 0;
			} else {
				return currentIndex + 1;
			}
		}

		function getPreviousItem() {
			if (currentIndex == 0) {
				return items.length-1;
			} else {
				return currentIndex-1;
			}
		}

		function setCurrent(index) {
			currentIndex = index;
			angular.forEach(indicators, function(item, i) {
				item.removeClass('active');
				if (i==index) {
					item.addClass('active')
				}
			})
		}

		$scope.next = function() {
			animate.next(getNextItem(), currentIndex);
			setCurrent(getNextItem());
			updateNumbers()
		};

		$scope.previous = function() {
			animate.previous(getPreviousItem(), currentIndex);
			setCurrent(getPreviousItem());
			updateNumbers()
		};

		$scope.debounceNext = _throttle($scope.next, 500);
		$scope.debouncePrevious = _throttle($scope.previous, 500);

		var animate = {
			next: function(sliderIn, sliderOut) {
				sliderIn = items[sliderIn].sliderParent[0];
				sliderOut = items[sliderOut].sliderParent[0];

				Velocity(sliderOut, 'stop');
				Velocity(sliderOut, {
					translateX: '-100%'
				}, {
					ease: animateEase,
					complete: function(el) {
						el[0].style.display = 'none';
					}
				});

				Velocity(sliderIn, 'stop');
				Velocity(sliderIn, {
					translateX: '100%'
				}, {
					duration: 0
				});
				Velocity(sliderIn, {
					translateX: '0'
				}, {
					ease: animateEase,
					begin: function(el) {
						el[0].style.display = 'block';
					}
				});
			},
			previous: function(sliderIn, sliderOut) {
				sliderIn = items[sliderIn].sliderParent[0];
				sliderOut = items[sliderOut].sliderParent[0];

				Velocity(sliderOut, 'stop');
				Velocity(sliderOut, {
					translateX: '100%'
				}, {
					ease: animateEase,
					complete: function(el) {
						el[0].style.display = 'none';
					}
				});

				Velocity(sliderIn, 'stop');
				Velocity(sliderIn, {
					translateX: '-100%'
				}, {
					duration: 0
				});
				Velocity(sliderIn, {
					translateX: '0'
				}, {
					ease: animateEase,
					begin: function(el) {
						el[0].style.display = 'block';
					}
				})
			},
			progress: function() {
				Velocity(bar[0], 'stop');
				bar[0].style.width = 0;
				Velocity(bar[0], {
					width: '100%'
				}, {
					duration: opts.duration,
					easing: 'linear'
				})
			},
			progressStop: function() {
				Velocity(bar[0], 'stop');
				bar[0].style.width = 0;
			}
		};

		this.addItem = function(element, background, coverImg, backgroundColor) {

			var sliderItemParent = angular.element(templates.sliderItemParent),
					backgroundItem = angular.element(templates.imageItem);

			$element.append(sliderItemParent);
			sliderItemParent.append(backgroundItem);
			sliderItemParent.append(element);
			backgroundItem[0].style.backgroundImage = `url('${$scope.backgroundScale?$filter('scaleImage')(background, $scope.backgroundScale):$filter('scaleImage')(background, 650)}')`;
			backgroundItem[0].style.opacity = opts.backdropOpacity;
			backgroundItem[0].style.backgroundColor = backgroundColor;

			if (coverImg) {
				let coverImgItem = angular.element(templates.coverImg);
				coverImgItem[0].style.backgroundImage = `url(${$scope.backgroundScale?$filter('scaleImage')(coverImg, $scope.backgroundScale):$filter('scaleImage')(coverImg, 650)})`;
				backgroundItem.append(coverImgItem);
			}

			items.push({
				sliderParent: sliderItemParent,
				sliderContent: element,
				sliderBackground: backgroundItem
			});

			if (items.length == 1) {
				start();
				noImage.remove();
			}
			if (items.length == 2) addMulti();
			if ($scope.numbersParent) {
				updateNumbers();
			}
		};

		$scope.indicationClick = function(goTo) {
			if (goTo != currentIndex) {
				if (goTo > currentIndex) {
					animate.next(goTo, currentIndex);
				} else {
					animate.previous(goTo, currentIndex);
				}
				setCurrent(goTo)
			}
		};

		this.addIndicators = function(indicatorParent) {
			$scope.indicatorParent = indicatorParent;
			angular.forEach(items, function(item, key) {
				var indication = angular.element(templates.indicator);
				indication.attr('ng-click', `indicationClick(${key})`);
				$compile(indication)($scope);
				indicators.push(indication);
				$scope.indicatorParent.append(indication);
				if (key==0) indication.addClass('active');
			});
			if (indicators.length<=1) {
				$scope.indicatorParent[0].style.display = 'none';
			}
		};

		this.addNumbers = function(numbersParent) {
			$scope.numbersParent = numbersParent;
			updateNumbers();
		};

		function updateNumbers() {
			if (!_isUndefined($scope.numbersParent)) {
				if (items.length > 1) {
					$scope.numbersParent[0].style.opacity = 1;
					$scope.numbersParent.html(currentIndex+1+'/'+items.length);
				} else {
					$scope.numbersParent[0].style.opacity = 0;
				}
			}
		}

		$scope.$on('$destroy', function() {
			$element[0].removeEventListener('mouseover', stopInterval);
			$element[0].removeEventListener('mouseleave', startInterval);
		});
	};

	return {
		restrict: 'E',
		scope: {
			backdropOpacity: '=',
			duration: '@',
			durationBar: '@',
			auto: '@',
			backgroundColor: '@',
			backgroundScale: '@'
		},
		controller: sliderCtrl
	}
};