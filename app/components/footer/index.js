module.exports = function() {
	let footerCtrl = function() {
		let vm = this;
		vm.date = Date.now();
	};

	let template = `
			<div class="footer">
				<div class="container">
					<div class="pull-left">
						<svg version="1.1" class="footer__insignia" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 23 26" style="enable-background:new 0 0 23 26;" xml:space="preserve"><style type="text/css">.st0{fill:#91AFC3;}</style><g><g><polygon class="st0" points="18.7,17.1 14.7,19.4 14.7,16.8 13.4,16.1 13.4,21.8 20.2,17.9 20.2,14.1 14.7,11 14.7,11 14.7,6.6 20.2,9.8 20.2,8.1 13.4,4.2 13.4,11.7 18.7,14.7 "/><path class="st0" d="M11.5,0L0,6.5v13L11.5,26L23,19.5v-13L11.5,0z M10.9,6.6l-5.3,3v6.7L7,17.2v-6.9l3.9-2.2v15.8l-9.4-5.3V7.3l9.4-5.3V6.6z M21.5,11.9L16,8.8v1.5l5.5,3.1v5.3l-9.4,5.3v-2.8v-1.8v-5.5l4.8,2.7v-1.4l-4.8-2.7V2.1l9.4,5.3V11.9z"/><polygon class="st0" points="9.6,10.3 8.3,11 8.3,19.4 4.3,17.1 4.3,8.9 9.6,5.9 9.6,4.2 2.8,8.1 2.8,17.9 9.6,21.8 "/></g></g></svg>
						<span>Game Slate &copy; 2015 - {{footer.date | date: 'yyyy'}}</span>
					</div>
					<div class="pull-right">
						<a ui-sref="about">About Game Slate</a>
					</div>
				</div>
			</div>
		`;

	return {
		restrict: 'E',
		controller: footerCtrl,
		controllerAs: 'footer',
		template: template
	};
};