let _isUndefined = require('lodash/isUndefined');
let _forEach = require('lodash/forEach');
import state from '../../core/state';

module.exports = function() {
	let gameScoreChooserCtrl = function($scope, gameSvc, userSvc) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, this);
		vm.state.set({
			displayScore: null,
			displayBool: false,
			user: angular.copy(vm.bindUsers)[0],
			community: {
				component: 'loading', // loading | hasData | error
				data: {}
			}
		});
		
		let getStats = () => {
			vm.state.user.component = 'loading';
			gameSvc.getStats(vm.gameUri, vm.state.user._id).then(
					res => {
						vm.state.set({
							user: {
								_id: vm.state.user._id,
								username: vm.state.user.username,
								data: res,
								component: 'hasData'
							}
						});
					},
					error => {
						vm.state.set({
							user: {
								_id: vm.state.user._id,
								username: vm.state.user.username,
								data: null,
								component: 'error'
							}
						});
					}
			);
		};

		// gameId is required
		if (_isUndefined(vm.gameUri)) {
			console.error(`[game-score-chooser] attribute 'game-uri' required`);
			return false;
		}

		// get game stats
		gameSvc.getStats(vm.gameUri).then(
				res => {
					vm.state.set({
						community: {
							data: res,
							component: 'hasData'
						}
					});
				},
				error => {
					vm.state.set({
						component: 'error'
					});
				}
		);

		$scope.$watch(function() {
			return vm.state.users;
		}, function(newVal) {
			getStats();
		});


		// view functions
		vm.showScore = function(user) {
			if (user) {
				vm.state.displayScore = user;
			} else {
				vm.state.displayScore = vm.state.community;
			}
			vm.state.displayBool = true;
		};

		vm.close = function() {
			setTimeout(() => {
				vm.state.set({
					displayBool: false
				});
			}, 100);
		};
	};

	return {
		restrict: 'E',
		scope: {
			bindUsers: '=users',
			gameUri: '=',
			onChange: '&'
		},
		template: require('./game-score-chooser.html'),
		controller: gameScoreChooserCtrl,
		controllerAs: 'gameScoreChooser',
		bindToController: true
	};
};