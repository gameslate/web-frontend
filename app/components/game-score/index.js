module.exports = function(app) {
	let gameScore = require('./game-score');
	let gameScoreChooser = require('./game-score-chooser');

	app.directive('gameScore', gameScore);
	app.directive('gameScoreChooser', gameScoreChooser);
};