import _isObject from 'lodash/isObject';
import _isUndefined from 'lodash/isUndefined';
import _isEmpty from 'lodash/isEmpty';
import _isNull from 'lodash/isNull';
import state from '../../core/state';

module.exports = function(userSvc, gameSvc, $rootScope, toasterSvc, rateGameSvc, authSvc) {
"ngInject";
	var gameScoreCtrl = function($scope) {
		"ngInject";

		let vm = this;
		vm.state = new state($scope, vm);
		vm.state.set({
			data: vm.data?vm.data:{},
			component: 'loading' // loading | hasData | noScore | error
		});

		function getStats(gameUri, userId) {
			gameSvc.getStats(gameUri, userId).then(
					res => {
						if (_isNull(res) || _isEmpty(res)) {
							vm.state.set({
								component: 'noScore'
							});
						} else {
							vm.state.set({
								data: res,
								component: 'hasData'
							});
						}
					},
					error => {
						vm.state.set({component: 'error'});
					}
			);
		}

		let figureOutWhichStats = () => {
			// if the data isn't passed in, get it
			if (!_isObject(vm.data) || _isEmpty(vm.data)) {
				vm.state.set({component: 'loading'});

				if (!_isUndefined(vm.userId) && !_isUndefined(vm.game)) {
					if (vm.userId == 'me') {
						getStats(vm.game.uri, authSvc.getUser()._id);
					} else {
						getStats(vm.game.uri, vm.userId);
					}
				} else if (!_isUndefined(vm.game)) {
					getStats(vm.game.uri);
				} else {
					console.error(`[gameScore] userId and/or gameId attributes are required`);
				}

			} else {
				vm.state.set({component: 'hasData'});
			}
		};

		$scope.$on('gameScoreUpdate', () => {
			figureOutWhichStats();
		});

		$scope.$on('scoreThisGame', () => {
			if (vm.userId == 'me') {
				vm.scoreThisGame();
			}
		});

		// run it the first time through
		figureOutWhichStats();

		$scope.$watch(() => {
			return vm.data;
		}, newVal => {
			vm.state.set({data: newVal});
		});

		vm.scoreThisGame = () => {
			if (authSvc.isAuthenticated()) {
				rateGameSvc.open(vm.game.uri, vm.state.data).then(
						res => {
							toasterSvc.info(`Successfully saved your score`);
							$rootScope.$broadcast('gameScoreUpdate');
							$scope.$emit('userScored');
						}
				);
			} else {
				authSvc.signInModal({message: 'You must be signed in to score this game.', title: 'Whoops'});
			}
		};

	};

	return {
		restrict: 'E',
		scope: {
			game: '=gameObject',
			userId: '@', // 'me' | INT
			data: '='
		},
		controller: gameScoreCtrl,
		controllerAs: 'gameScore',
		bindToController: true,
		template: require('./game-score.html')
	}
};