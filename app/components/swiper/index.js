module.exports = function(app) {
	app.directive('swiper', function() {
		let ctrl = function($element) {
			"ngInject";
			let vm = this;
			vm.options = vm.bindOptions;
		};

		return {
			restrict: 'E',
			replace: true,
			scope: {bindOptions: '=options'},
			controller: ctrl,
			controllerAs: 'swiper',
			bindToController: true
		};
	});
};