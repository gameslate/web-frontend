# Game Slate Front-End

### Setup Dev Environment

 - install ruby and sass gem
 - install nodejs (make sure you're running npm 3.x.x+)
 - clone repo
 - run `npm install webpack bower -g`
 - run `npm install`
 - run `bower install` (it may ask you which versions to use - always choose the latest)


### Start Dev Environment (Mac/Linux)

run `npm start`


### Start Dev Environment (Windows)

 - Manually state NODE_ENV by running `set NODE_ENV=development`
 - run `npm run start-w`