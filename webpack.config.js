var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var envSettings = new webpack.DefinePlugin({
	__DEV__: (process.env.NODE_ENV === 'development' || !process.env.NODE_ENV),
	__PROD__: process.env.NODE_ENV === 'production'
});

var htmlPlugin = new HtmlWebpackPlugin({
	template: 'app/index.ejs',
	production: process.env.NODE_ENV === 'production',
	hash: true
});

var extractPlugin = new ExtractTextPlugin('main.css');

var config = {
	entry: {
		app: path.resolve(__dirname, 'app', 'app.js')
	},
	output: {
		path: path.resolve(__dirname, 'app'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'ng-annotate!babel?presets[]=es2015',
				exclude: [/node_modules/, /lib/]
			},
			{
				test: /\.json$/,
				loader: 'json',
				exclude: [/node_modules/, /lib/]
			},
			{
				test: require.resolve('angular'),
				loader: 'expose?angular'
			},
			{
				test: /.html$/,
				loader: 'raw',
				exclude: [/node_modules/, /lib/]
			},
			{
				test: /.scss$/,
				loader: 'style!css!postcss-loader!sass',
				exclude: [/node_modules/, /lib/]
			}
		]
	},
	resolve: {
		alias: {
			'~': path.resolve(__dirname, 'app')
		}
	},
	plugins: [envSettings, htmlPlugin, extractPlugin],
	postcss: function() {
		return [require('autoprefixer')];
	}
};

if (process.env.NODE_ENV === 'development') {

}


if (process.env.NODE_ENV === 'production') {
	config.output.path = path.resolve(__dirname, 'dist');
	config.module.loaders[4].loader = ExtractTextPlugin.extract('style-loader', 'css!postcss-loader!sass');

	config.devtool = 'source-map';
	config.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = config;