module.exports = {
	"docs":[
		{
			"_id":"56846112fabab7c50ce5248b",
			"title":"Batman: Arkham Knight",
			"uri":"batman-arkham-knight",
			"originalReleaseDate":"2015-06-23T04:00:00.000Z",
			"description":"Arkham Knight, developer Rocksteady's return to the Batman series, takes place one year after Arkham City. It promises to expand the open world from the previous game, allowing players to drive the Batmobile through Gotham City's streets.",
			"esrb":"",
			"platforms":[
				{
					"name":"Mac",
					"abbreviation":"MAC",
					"platformId":"567ad470ce465e480ad4bf74",
					"_id":"56846112fabab7c50ce52490"
				},
				{
					"name":"PC",
					"abbreviation":"PC",
					"platformId":"567ad455ce465e480ad4bf4a",
					"_id":"56846112fabab7c50ce5248f"
				},
				{
					"name":"Xbox One",
					"abbreviation":"XONE",
					"platformId":"567ad455ce465e480ad4bf49",
					"_id":"56846112fabab7c50ce5248e"
				},
				{
					"name":"PlayStation 4",
					"abbreviation":"PS4",
					"platformId":"567ad455ce465e480ad4bf4f",
					"_id":"56846112fabab7c50ce5248d"
				},
				{
					"name":"Linux",
					"abbreviation":"LIN",
					"platformId":"56846112fabab7c50ce5248a",
					"_id":"56846112fabab7c50ce5248c"
				}
			],
			"genres":[
				{
					"id":1234,
					"name":"Adventure"
				},
				{
					"id":2345,
					"name":"First-Person Shooter",
					"abbreviation":"FPS"
				}
			],
			"images":[
				{
					"url":"http://res.cloudinary.com/game-slate/image/upload/v1451528248/batman-arkham-knigh-screenshot_erhm52.jpg",
					"title":"Batman: Arkham Knight Screenshot"
				}
			],
			"id":"56846112fabab7c50ce5248b"
		}
	],
	"total":1,
	"limit":10,
	"page":1,
	"pages":1,
	"sort":{
		"originalReleaseDate":-1
	}
};