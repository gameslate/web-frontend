module.exports = {
	"id": 1234,
	"score": {
		"total": 88,
		"gameplay": 90,
		"musicSound": 89,
		"controls": 88,
		"story": 87,
		"graphics": 86,
		"replayValue": 85
	},
	"playTime": 30
};