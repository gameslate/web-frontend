module.exports = {
	"docs": [
		{
			"id": "4a772b6d-ba26-4833-b0c8-101837ed254b",
			"name": "Batman's Last Ride",
			"uri": "batmans-last-ride",
			"author": {
				"id": 123,
				"name": "Tony Lefler",
				"handle": "geoctrl",
				"profileImgUrl": null
			},
			"lead": "Can Batman's last game live up to the hype?",
			"preview": "Lorem ipsum dolor sit amet, ex mei vocent tacimates molestiae. Usu id incorrupte percipitur, an sea ludus assentior scribentur. Alii fastidii ne sit. Cu vis modus inani virtute, quod labitur philosophia mei in, choro omnium ut pro.",
			"coverImgUrl": "http://gearnuke.com/wp-content/uploads/2015/06/arkhamknight3.jpg",
			"tags": [
				"super awesome game",
				"another tag!"
			],
			"published": 1453867131748,
			"viewed": 553,
			"rating": {
				"total": 95,
				"up": 110,
				"down": 15
			},
			"game": {
				"id": "682657fa-88e2-4f6e-893d-610299ece34e",
				"name": "Batman: Arkham Knight",
				"uri": "batman-arkham-knight"
			}
		},
		{
			"id": "d3fc5228-2575-4865-a8ba-cc8e34d1a24c",
			"name": "Batman's Last Ride",
			"uri": "batmans-last-ride",
			"author": {
				"id": 444,
				"name": "John Doe",
				"handle": "johnDoe",
				"profileImgUrl": null
			},
			"lead": null,
			"preview": "Lorem ipsum dolor sit amet, ex mei vocent tacimates molestiae. Usu id incorrupte percipitur, an sea ludus assentior scribentur. Alii fastidii ne sit. Cu vis modus inani virtute, quod labitur philosophia mei in, choro omnium ut pro...",
			"coverImgUrl": "http://vignette2.wikia.nocookie.net/batman/images/2/26/TheScarecrow-Arkham_Knight.jpg/revision/latest?cb=20140611195625",
			"tags": [
				"super awesome game",
				"another tag!"
			],
			"published": 1453867131748,
			"viewed": 553,
			"rating": {
				"total": 95,
				"up": 110,
				"down": 15
			},
			"game": {
				"id": "682657fa-88e2-4f6e-893d-610299ece34e",
				"name": "Batman: Arkham Knight",
				"uri": "batman-arkham-knight"
			}
		},
		{
			"id": "d3fc5228-2575-4865-a8ba-cc5555d1a24c",
			"name": "Not your Average PC Game... And that's not a good thing...",
			"uri": "batmans-last-ride",
			"author": {
				"id": 444,
				"name": "Jane Doe",
				"handle": "janeDoe",
				"profileImgUrl": null
			},
			"lead": "An important look at the decline of Warner gaming including sister games such as \"Shadow of Mordor\", etc.",
			"preview": "Lorem ipsum dolor sit amet, ex mei vocent tacimates molestiae. Usu id incorrupte percipitur, an sea ludus assentior scribentur. Alii fastidii ne sit. Cu vis modus inani virtute, quod labitur philosophia mei in, choro omnium ut pro.",
			"coverImgUrl": "http://static3.gamespot.com/uploads/scale_large/104/1049837/2891179-batman-arkham_knight-review_nologo_20150618.jpg",
			"tags": [
				"super awesome game",
				"another tag!"
			],
			"published": 1453867131748,
			"viewed": 553,
			"rating": {
				"total": 95,
				"up": 110,
				"down": 15
			},
			"game": {
				"id": "682657fa-88e2-4f6e-893d-610299ece34e",
				"name": "Batman: Arkham Knight",
				"uri": "batman-arkham-knight"
			}
		}
	],
	"total":1,
	"limit":10,
	"page":1,
	"pages":1
};