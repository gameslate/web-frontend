module.exports = {
	"id": 1234,
	"score": {
		"total": 79,
		"gameplay": 90,
		"musicSound": 89,
		"controls": 88,
		"story": 87,
		"graphics": 86,
		"replayValue": 55
	},
	"playTime": 30
};