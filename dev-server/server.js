var path = require('path');

function getResource(p) {
	return require('./resources/' + p);
}

require('bogus-api').create({
	resourceDir: path.resolve(__dirname, './resources'),
	resourceUriPrefix: '/api/v1',

	priorityRoutes: function(server) {

		// ----- GAMES
		server.get('/api/v1/games/:id', function(req, res) {
			res.status(200).json(getResource('games_id_GET'));
		});

		server.get('/api/v1/games/:id/stats', function(req, res) {
			res.status(200).json(getResource('games_id_stats_GET'));
		});

		server.get('/api/v1/games?search=:term', function(req, res) {
			res.status(200).json(getResource('games_id_stats_GET'));
		});


		// ----- STATS
		server.get('/api/v1/stats/:id', function(req, res) {
			res.status(200).json(getResource('stats_id_GET'));
		});


		// ----- SLATES
		server.get('/api/v1/slates?search', function(req, res) {
			res.status(200).json(getResource('slates_search_GET'));
		});

		server.get('/api/v1')

	}
}).start();